import {Troubleshooting} from "../classes/troubleshooting";

export interface IQuestionResponse {
  question : string;
  response : string;
}


export class QuestionResponse implements IQuestionResponse {
  id : number;
  question: string;
  response: string;
  troublshooting : Troubleshooting;
}
