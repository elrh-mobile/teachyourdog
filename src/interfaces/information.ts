export interface InformationInterface {
  id : number;
  title : string;
  description : string;
}
