import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Tutorial} from "../../classes/tutorial";
import { ITutorialService } from '../../providers/tutorial/tutorial-interface';

/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
//*******************************************************************************************************
  tutorial : Tutorial;
  tutorialParam : any;
  teachItPage = 'TeachItPage';
  detailsPage = 'DetailsPage';
  troubleshooting = 'TroubleshootingPage';
  title : string;
//*******************************************************************************************************
  constructor(public navCtrl: NavController, public navParams: NavParams,private tutorialService : ITutorialService) {

  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.getTutorial();
  }
//*******************************************************************************************************
  private getTutorial(){
    console.log('ionViewDidLoad TutorialPage');
    this.tutorial = this.tutorialService.getTutoNavParam();
    this.title = this.tutorial.title;
  }
//*******************************************************************************************************
}
