import {Component, ViewChild} from '@angular/core';
import { NavController, Slides } from 'ionic-angular';
import {AdvertisingProvider} from "../../providers/advertising/advertising";
import {IMG_ROOT_FOLDER} from "../../app/config";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
//*******************************************************************************************************
  public imgFolder = IMG_ROOT_FOLDER+'home/';
  @ViewChild(Slides) slides: Slides;
//*******************************************************************************************************
  constructor(
              public navCtrl: NavController,
              private adsProvider : AdvertisingProvider
  ) {

  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.initAds();
  }
//*******************************************************************************************************
  ionViewWillLeave(){
  }
//*******************************************************************************************************
  private async initAds(){
    await this.adsProvider.disableInterstitial();
    await this.adsProvider.disableBanner();
  }
}
