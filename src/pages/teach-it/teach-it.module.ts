import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeachItPage } from './teach-it';
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
    TeachItPage,
  ],
  imports: [
    IonicPageModule.forChild(TeachItPage),
    SharedModule
  ]
})
export class TeachItPageModule {}
