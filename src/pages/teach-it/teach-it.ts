import { HomePage } from './../home/home';
import { NetworkProvider } from './../../providers/network/network';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Tutorial} from "../../classes/tutorial";
import {ITutorialService} from "../../providers/tutorial/tutorial-interface";
import {AdvertisingProvider} from "../../providers/advertising/advertising";

/**
 * Generated class for the TeachItPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teach-it',
  templateUrl: 'teach-it.html',
})
export class TeachItPage {
//*******************************************************************************************************
  public tutorial : Tutorial;
//*******************************************************************************************************
  constructor(public navCtrl: NavController, private adsProvider : AdvertisingProvider,
              public navParams: NavParams,public tutosService: ITutorialService,
              private networkProvider : NetworkProvider) {
        this.tutorial = this.navParams.get('tuto-param');
  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.tutorial = this.tutosService.getTutoNavParam();
    if(this.tutorial !=  null && this.tutorial.troubleshooting == null){
      console.log(' we load teachit of tuto');
      this.loadTeachIt();
    }
  }
//*******************************************************************************************************
  ionViewDidEnter(){
    this.initAds();
    if(!this.networkProvider.isConnected()){
      this.networkProvider.presentNoNetworkAlert(HomePage);
      return false;
    }
  }
//*******************************************************************************************************
  ionViewWillLeave(){
  }
//*******************************************************************************************************
  private async initAds(){
    this.adsProvider.enableInterstitial();
    if((await this.adsProvider.startInterstitial()))
      await this.adsProvider.hideBanner();
  }
//*******************************************************************************************************
  async loadTeachIt(){
    return await this.tutosService.setTeachItOfTutorial(this.tutorial);
  }
//*******************************************************************************************************
}
