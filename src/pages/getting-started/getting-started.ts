import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdvertisingProvider } from '../../providers/advertising/advertising';
import { IMG_ROOT_FOLDER } from '../../app/config';

/**
 * Generated class for the GettingStartedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-getting-started',
  templateUrl: 'getting-started.html',
})
export class GettingStartedPage {
//*******************************************************************************************************
  public imgFolder = IMG_ROOT_FOLDER+'getting-started/';
  constructor(public navCtrl: NavController, public navParams: NavParams,private adsProvider:AdvertisingProvider) {
  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.initAds();
  }
//*******************************************************************************************************
  ionViewWillLeave(){
  }
//*******************************************************************************************************
  private async initAds(){
    await this.adsProvider.disableInterstitial();
    await this.adsProvider.disableBanner();
  }
//*******************************************************************************************************
}
