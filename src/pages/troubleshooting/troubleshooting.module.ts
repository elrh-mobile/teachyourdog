import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TroubleshootingPage } from './troubleshooting';
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
    TroubleshootingPage,
  ],
  imports: [
    IonicPageModule.forChild(TroubleshootingPage),
    SharedModule
  ]
})
export class TroubleshootingPageModule {}
