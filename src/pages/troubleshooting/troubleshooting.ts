import { HomePage } from './../home/home';
import { NetworkProvider } from './../../providers/network/network';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Tutorial} from "../../classes/tutorial";
import {ITutorialService} from "../../providers/tutorial/tutorial-interface";
import { AdvertisingProvider } from '../../providers/advertising/advertising';

/**
 * Generated class for the TroubleshootingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-troubleshooting',
  templateUrl: 'troubleshooting.html',
})
export class TroubleshootingPage {
//*******************************************************************************************************
  public tutorial : Tutorial;
//*******************************************************************************************************
  constructor(public navCtrl: NavController, public navParams: NavParams,public tutosService: ITutorialService,
              public adsProvider : AdvertisingProvider,private networkProvider : NetworkProvider) {

  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.tutorial = this.tutosService.getTutoNavParam();
    if(this.tutorial !=  null && this.tutorial.troubleshooting == null){
      this.loadTroublshooting();
    }
    console.log(this.tutorial);
    this.initAds();
  }
//*******************************************************************************************************
  ionViewDidEnter(){
    if(!this.networkProvider.isConnected()){
      this.networkProvider.presentNoNetworkAlert(HomePage);
      return false;
    }

  }
//*******************************************************************************************************
  ionViewWillLeave(){
  }
//*******************************************************************************************************
  async loadTroublshooting(){
    return await this.tutosService.setTroublshootingOfTutorial(this.tutorial);
  }
//*******************************************************************************************************
  private async initAds(){
    await this.adsProvider.startBanner();
  }
}
