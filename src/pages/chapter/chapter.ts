import { HomePage } from './../home/home';
import { NetworkProvider } from './../../providers/network/network';
import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import {Chapter} from "../../classes/chapter";
import {Tutorial} from "../../classes/tutorial";
import {AdvertisingProvider} from "../../providers/advertising/advertising";
import { ITutorialService } from '../../providers/tutorial/tutorial-interface';
import {IChapterService} from "../../providers/chapter/chapter-interface";

/**
 * Generated class for the ChapterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chapter',
  templateUrl: 'chapter.html',
})
export class ChapterPage {
//*******************************************************************************************************
  public chapter:Chapter;
//*******************************************************************************************************
  constructor(public navCtrl: NavController, private tutorialService : ITutorialService,
              public navParams: NavParams,private adsProvider:AdvertisingProvider,
              private networkProvider : NetworkProvider,private chapterService : IChapterService) {

  }
//*******************************************************************************************************
  ionViewDidLoad() {
    this.chapter = this.chapterService.getChapterNavParam();
    if(this.chapter == null)
      this.navCtrl.setRoot(HomePage);
    this.initAds();
  }
//*******************************************************************************************************
  ionViewDidEnter() {
    if(!this.networkProvider.isConnected()){
      this.networkProvider.presentNoNetworkAlert(HomePage);
      return false;
    }
  }
//*******************************************************************************************************
  ionViewWillLeave(){
  }
//*******************************************************************************************************
  private async initAds(){
    this.adsProvider.disableInterstitial();
    // re-prepare banner
    this.adsProvider.enableBanner();
    await this.adsProvider.prepareBanner();
    await this.adsProvider.startBanner();
  }
//*******************************************************************************************************
  goToTutorial(tutorial : Tutorial){
    if( ! this.networkProvider.isConnected()){
      this.networkProvider.presentNoNetworkAlert(HomePage);
      return false;
    }
    this.tutorialService.setTutoNavParam(tutorial);
    this.navCtrl.push('TutorialPage');
  }
//*******************************************************************************************************
}
