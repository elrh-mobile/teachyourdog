import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ChapterPage } from './chapter';
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
    ChapterPage,
  ],
  imports: [
    IonicPageModule.forChild(ChapterPage),
    SharedModule
  ],
  exports : [
    ChapterPage
  ]
})
export class ChapterPageModule {}
