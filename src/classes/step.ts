import {StepInterface} from "../interfaces/step";
import {TutorialDetails} from "./tutorial-details";

export class Step implements StepInterface {
  number: number;
  description: string;
  tutoDetails : TutorialDetails;
}
