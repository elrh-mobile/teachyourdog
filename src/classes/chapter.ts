import {InformationInterface} from "../interfaces/information";
import {ImageInterface} from "../interfaces/image";
import {Tutorial} from "./tutorial";
import {IMG_ROOT_FOLDER} from "../app/config";

export class Chapter implements InformationInterface,ImageInterface{
  id : number;
  number : number;
  title : string;
  private _img : string = null;
  description : string;
  tutorials : Tutorial[] = [];

  getImgURL(): string {
    if(this.id != null)
      return IMG_ROOT_FOLDER+'chapters/'+this.number+'/';
    return null;
  }

  get img(): string {
    if(this._img == null){
      this._img = this.getImgURL()+this.number + '.jpg';
    }
    return this._img;
  }

  set img(value: string) {
    this._img = value;
  }
}
