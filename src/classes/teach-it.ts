import {InformationInterface} from "../interfaces/information";
import {ImageStep} from "./image-step";
import {Tutorial} from "./tutorial";

export class TeachIt implements InformationInterface {
  id: number;
  title: string;
  description: string;
  steps : ImageStep[];
  private _tutorial : Tutorial;


  get tutorial(): Tutorial {
    return this._tutorial;
  }

  set tutorial(value: Tutorial) {
    this._tutorial = value;
  }
}
