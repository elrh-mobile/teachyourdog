import {InformationInterface} from "../interfaces/information";
import {ImageInterface} from "../interfaces/image";
import {TeachIt} from "./teach-it";
import {TutorialDetails} from "./tutorial-details";
import {Troubleshooting} from "./troubleshooting";
import {Chapter} from "./chapter";

export class Tutorial implements InformationInterface,ImageInterface{
  id: number;
  number : number;
  title: string;
  description: string;
  private _img: string = null;
  //
  teachIt : TeachIt;
  tutorialDetails : TutorialDetails;
  troubleshooting : Troubleshooting;
  // chapter
  chapter : Chapter;

  getImgURL(): string {
    if(this.chapter != null && this.number != null){
      return this.chapter.getImgURL()+'tuto'+this.number+'/';
    }
    return null;
  }


  get img(): string {
    if(this._img == null){
      this._img = this.getImgURL()+'tuto'+this.number+'.jpg';
    }
    return this._img;
  }

  set img(value: string) {
    this._img = value;
  }
}
