import {InformationInterface} from "../interfaces/information";
import {Step} from "./step";
import {Tutorial} from "./tutorial";

export class TutorialDetails implements InformationInterface{
  id: number;
  title: string;
  description: string;
  steps : Step[];
  tutorial : Tutorial;
}
