import {StepInterface} from "../interfaces/step";
import {ImageInterface} from "../interfaces/image";
import {TeachIt} from "./teach-it";

export class ImageStep implements StepInterface,ImageInterface {
  number: number;
  description: string;
  private _img: string = null;
  teachIt : TeachIt;

  getImgURL(): string {
    return this.img;
  }

  get img(): string {
    if(this._img == null){
      this._img = this.teachIt.tutorial.getImgURL()+this.number+'.jpg';
    }
    return this._img;
  }


  set img(value: string) {
    this._img = value;
  }
}
