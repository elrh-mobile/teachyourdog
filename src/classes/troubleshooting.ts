import {InformationInterface} from "../interfaces/information";
import {QuestionResponse} from "../interfaces/question-response";
import {Tutorial} from "./tutorial";

export class Troubleshooting implements InformationInterface{
  id: number;
  title: string;
  description: string;
  tutorial :Tutorial;
  qrs : QuestionResponse[] = [];
}
