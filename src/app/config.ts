/**
 * ROOT FOLDER THAT CONTAINS ALL IMGS
 * @type {string}
 */
export const IMG_ROOT_FOLDER = 'assets/imgs/';
/**
 * number of secondes to show interstital if we are in teachit page
 * @type {number}
 */
export const INTERSTITAL_INTERVAL = 60 * 3;
