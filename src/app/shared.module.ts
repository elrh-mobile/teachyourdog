import { ComponentsModule } from '../components/components.module';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
  ],
  imports: [
    ComponentsModule,
    IonicModule
  ],
  exports: [
    ComponentsModule,
    IonicModule
  ]
})

export class SharedModule { }
