import { HomePage } from './../pages/home/home';
import { NetworkProvider } from './../providers/network/network';
import { Component, ViewChild } from '@angular/core';
import { LoadingController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Chapter} from "../classes/chapter";
import {IChapterService} from "../providers/chapter/chapter-interface";
import {DatabaseProvider} from "../providers/database/database-provider";
import {IMG_ROOT_FOLDER} from "./config";
import {AdvertisingProvider} from "../providers/advertising/advertising";



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
//**********************************************************************************************************************
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;
  pages: Array<{title: string, component: any}>;
  public chapters : Chapter[] = [];
  public chargement : boolean = true;
  public imgFolder = IMG_ROOT_FOLDER+'menu/';
  private bannerHidden : boolean = false;
//**********************************************************************************************************************
  constructor
  (
    public dataBaseProvider : DatabaseProvider, public platform: Platform, public statusBar: StatusBar,
    public splashScreen: SplashScreen, public chapterService : IChapterService,public loadingCtrl: LoadingController,
    private networkProvider : NetworkProvider,private adsProvider : AdvertisingProvider
  ) {
    this.initializeApp();
  }
//**********************************************************************************************************************
  async initializeApp() {
    this.platform.ready().then(async () => {
           let loading = this.loadingCtrl.create({
              spinner : 'crescent',
              content: 'App Configuration, It will take 1 or 2 minutes, Please wait...',
              enableBackdropDismiss : false,
            });
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            let r: boolean = true;
            // if the app is not ready => show loading
            if(!(await this.dataBaseProvider.isReadyDatabase()) || ! (await this.dataBaseProvider.isReadyTables())) {
              await loading.present();
              r = await this.dataBaseProvider.initDatabase();
              await loading.dismiss();
              this.chargement = false;
            }
            // get all chapter
            if (r) {
              this.chapters = await this.chapterService.getAllChapter();
            }
            // network
            console.log(this.networkProvider.isConnected());
    });
  }
//**********************************************************************************************************************
  public goToHomePage(){
    this.nav.setRoot(HomePage);
  }
//**********************************************************************************************************************
  public goToPage(page : any, params : any){
    this.nav.push(page, params);
  }
  async onOpenMenu(){
    if(this.bannerHidden == false){
      console.log('HIDDING BANNER FROM MENU');
      try {
        let value: boolean = (await this.adsProvider.hideBanner());
        console.log(value);
        if (value != false) {
          console.log(value);
          console.log('banner hidden with menu');
          this.bannerHidden = true;
        }
      }
      catch (e){
        this.bannerHidden = false;
      }
    }
  }

  async onCloseMenu(){
    if(this.bannerHidden)
      this.adsProvider.startBanner()
        .then(() => {
          console.log('banner hidden with menu');
          this.bannerHidden = false;
        })
  }
//**********************************************************************************************************************
  openChapterPage(chapter : Chapter) {
    // check network
    if( ! this.networkProvider.isConnected()){
      this.networkProvider.presentNoNetworkAlert();
      return false;
    }
    this.chapterService.setChapterNavParam(chapter);
    this.nav.setRoot('ChapterPage',null);
  }
//**********************************************************************************************************************
}
