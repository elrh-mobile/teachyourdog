import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/Network';
import {SharedModule} from './shared.module';
import {StaticChapterProvider} from '../providers/chapter/static-chapter';
import {IChapterService} from '../providers/chapter/chapter-interface';
import {StaticTutorialProvider} from '../providers/tutorial/static-tutorial-provider';
import {ITutorialService} from '../providers/tutorial/tutorial-interface';
import {SQLite} from '@ionic-native/sqlite';
import {SqliteDatabaseProvider} from "../providers/database/sqlite-database-provider";
import {DatabaseProvider} from "../providers/database/database-provider";
import {IonicStorageModule} from "@ionic/storage";
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import {AdMobFree} from "@ionic-native/admob-free";
import { AdvertisingProvider } from '../providers/advertising/advertising';
import { NetworkProvider } from '../providers/network/network';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    SharedModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StaticChapterProvider,
    {provide : IChapterService, useExisting: StaticChapterProvider},
    StaticTutorialProvider,
    {provide : ITutorialService, useExisting: StaticTutorialProvider},
    SqliteDatabaseProvider,
    {provide : DatabaseProvider, useExisting: SqliteDatabaseProvider},
    SQLitePorter,
    SQLite,
    AdMobFree,
    AdvertisingProvider,
    Network,
    NetworkProvider
  ]
})
export class AppModule {}
