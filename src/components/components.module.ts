import { NgModule } from '@angular/core';
import { DogAppHeaderComponent } from './dog-app-header/dog-app-header';
import {IonicModule} from "ionic-angular";
import { ChapterIntroductionComponent } from './chapter-introduction/chapter-introduction';
import { ListTutosComponent } from './list-tutos/list-tutos';

@NgModule({
	declarations: [DogAppHeaderComponent,
    ChapterIntroductionComponent,
    ListTutosComponent],
	imports: [IonicModule],
	exports: [DogAppHeaderComponent,
    ChapterIntroductionComponent,
    ListTutosComponent]
})
export class ComponentsModule {}
