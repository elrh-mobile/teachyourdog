import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Chapter} from "../../classes/chapter";
import {Tutorial} from "../../classes/tutorial";
import {IMG_ROOT_FOLDER} from "../../app/config";

/**
 * Generated class for the ChapterIntroductionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chapter-introduction',
  templateUrl: 'chapter-introduction.html'
})
export class ChapterIntroductionComponent {
  public imgFolder = IMG_ROOT_FOLDER+'chapters/';
  @Input()
  chapter: Chapter;

  @Output()
  onSelectTutorial : EventEmitter<Tutorial>;

  constructor() {
    console.log('Hello ChapterIntroductionComponent Component');
    this.onSelectTutorial = new EventEmitter();
  }

  saveAndSendTutorial(tutorial : Tutorial){
     console.log('we receiv '+ tutorial.id + ' in intro-chapter component');
      this.onSelectTutorial.emit(tutorial);
  }
}
