import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Tutorial} from "../../classes/tutorial";

/**
 * Generated class for the ListTutosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-tutos',
  templateUrl: 'list-tutos.html'
})
export class ListTutosComponent {

  @Input()
  listeTitle : string;

  @Input()
  tutos : Tutorial[] = [];

  @Output()
  onSelectTutorial : EventEmitter<Tutorial>;

  tutorial : Tutorial;
  constructor() {
    if(this.listeTitle === null || this.listeTitle === '')
      this.listeTitle = 'Tutorials';

    this.onSelectTutorial = new EventEmitter();
  }

  selectItem(tutorial : Tutorial){
    this.tutorial = tutorial;
    this.onSelectTutorial.emit(this.tutorial);
  }

}
