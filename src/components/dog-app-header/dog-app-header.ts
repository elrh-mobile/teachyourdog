import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

/**
 * Generated class for the DogAppHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'dog-app-header',
  templateUrl: 'dog-app-header.html'
})
export class DogAppHeaderComponent implements OnChanges{

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  @Input()
  title: string;

  constructor() {
  }

}
