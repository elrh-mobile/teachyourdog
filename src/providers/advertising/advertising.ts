import {Injectable} from '@angular/core';
import {AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig} from "@ionic-native/admob-free";
import {Platform} from "ionic-angular";
import {INTERSTITAL_INTERVAL} from "../../app/config";

/*
Advertising service
Banner :
========
enableBanner () : enable banner ads
disableBanner() : hide banner and disable ads
hideBanner() : hide banner if it shown
isShowing() : check if the banner is showing
prepareBanner() : prepare the banner if it is not yet ready
startBanner() :  show the banner if it is not showing and if it is ready
*/
@Injectable()
export class AdvertisingProvider {
//**********************************************************************************************************************
  /**
   * androidBannerId : 'ca-app-pub-7458022379926628/9590789537';
   * androidInterstitialId = 'ca-app-pub-7458022379926628/7794546039';
   */
  private interstitialId :string;
  private bannerId : string;
  private bannerConfig: AdMobFreeBannerConfig;
  private interstitialConfig : AdMobFreeInterstitialConfig;
//**********************************************************************************************************************
  private banner  : any  = {
    interval : 4000,
    id : 'ca-app-pub-5176932008237878/9186647585',
    state : true,
    ready : false
  };
//**********************************************************************************************************************
  private interstital  : any  = {
    // 75 seconds
    interval : INTERSTITAL_INTERVAL,
    state : false,
    showing : false,
    lastExecution : null,
    id : 'ca-app-pub-5176932008237878/8418105796'
  };
  private isAdvertisingEnabled = false;
//**********************************************************************************************************************
  constructor(private admobFree: AdMobFree,private platform : Platform) {
    // enable ads
    this.platform.ready().then(() => {
      // enable ads
      this.enableAds();
      this.enableBanner();
      if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
        this.bannerId = null;
        this.interstitialId = null;
      }
      // configure banner
      this.bannerConfig = {
        id: this.banner.id,
        size: 'SMART_BANNER',
        autoShow: false,
        isTesting: false,
        bannerAtTop: false,
        overlap: false,
        offsetTopBar: false
      };
      // configure inter
      this.interstitialConfig = {
        id: this.interstital.id,
        autoShow: true,
        isTesting: false
      };
      // set configuration
      this.admobFree.banner.config(this.bannerConfig);
      this.admobFree.interstitial.config(this.interstitialConfig);

      // disable ads when app is paused
      this.platform.pause.subscribe(() => {
          this.disableAds();
      });
      this.platform.resume.subscribe(() => {
          this.enableAds();
      });
  });
  }
//**********************************************************************************************************************
//**************************************************** BANNER **********************************************************
//**********************************************************************************************************************
  public async startBanner(){
    if(!this.isAdsEnabled()){
      console.log('Ads are  Disabled');
      return false;
    }
    if(!this.isBannerEnabled()){
      console.log('Banner is Disabled');
      return false;
    }
    //
    if(this.isBannerShowing()){
      console.log('Banner is already showing');
      return false;
    }
    //
    if(! this.isBannerReady()){
      await this.prepareBanner();
    }
    //
    try{
      await this.admobFree.banner.show();
      this.setBannerShowing(true);
      console.log('showing banner');
      return true;
    }
    catch(e){
      this.setBannerShowing(false);
      return false;
    }
  }
//**********************************************************************************************************************
public async prepareBanner(){
  if(!this.isAdsEnabled() || !this.isBannerEnabled()){
    console.log('Banner or Ads are disabled');
    return false
  }
  try{
    await this.admobFree.banner.prepare();
    console.log('preparing banner');
    this.setBannerReady(true);
    return true;
  }
  catch (e){
    this.setBannerReady(false);
    return false;
  }
}
//**********************************************************************************************************************
public async startBannerAfterTimeout(){
   await this.startBannerAfterTimeoutParam(this.banner.interval);
}
//**********************************************************************************************************************
public async startBannerAfterTimeoutParam(timeout : number){
  setTimeout( async () => {
     await this.startBanner();
  },timeout);
}
//**********************************************************************************************************************
public enableBanner(){
  this.banner.state = true;
}
//**********************************************************************************************************************
public async disableBanner(){
  await this.hideBanner();
  this.setBannerReady(false);
  this.setBannerShowing(false);
  this.setBannerState(false);
}
//**********************************************************************************************************************
public isBannerEnabled(){
  return this.banner.state == true;
}
//**********************************************************************************************************************
public isBannerShowing(){
  return this.banner.showing == true;
}
//**********************************************************************************************************************
private setBannerShowing(showing : boolean){
  this.banner.showing = showing;
}
//**********************************************************************************************************************
private isBannerReady() : boolean{
  return this.banner.ready == true;
}
//**********************************************************************************************************************
  public setBannerReady(ready : boolean) : boolean{
    return this.banner.ready = ready;
  }
//**********************************************************************************************************************
private setBannerState(state : boolean){
  this.banner.state = state;
}
//**********************************************************************************************************************
  public async hideBanner() : Promise<any>{
  if( !this.isBannerShowing()){
    console.log('WE WILL NOT HIDE BANNER');
    return Promise.resolve(false);
  }
  try {
    await this.admobFree.banner.hide();
    this.setBannerShowing(false);
    return Promise.resolve(true);
  } catch (e){
    return Promise.resolve(false);
  }
}
//**********************************************************************************************************************
//**************************************************** INTERS **********************************************************
//**********************************************************************************************************************
  public async startInterstitial (){
    if(! this.isAdsEnabled() || !this.isInterstitialEnbaled()){
      return false;
    }
    if(! this.checkInterstitalTimeExecution()){
      await this.startBanner();
      return false;
    }
    try {
      await this.admobFree.interstitial.prepare();
      await this.admobFree.interstitial.isReady();
      console.log('showing interstital');
      return true;
    } catch (e) {
      return false;
    }
  }
//**********************************************************************************************************************
public async startInterstitialAfterTimeout(){
  await this.startInterstitialAfterTimeoutParam(this.interstital.interval);
}
//**********************************************************************************************************************
public async startInterstitialAfterTimeoutParam(timeout){
  setTimeout(async () => {
      this.startInterstitial();
  },timeout);
}
//**********************************************************************************************************************
private enableAds(){
  this.isAdvertisingEnabled = true;
}
//**********************************************************************************************************************
  private disableAds(){
    this.isAdvertisingEnabled = false;
  }
//**********************************************************************************************************************
  private isAdsEnabled(){
    return this.isAdvertisingEnabled == true;
  }
//**********************************************************************************************************************
public disableInterstitial(){
  this.interstital.state = false;
}
//**********************************************************************************************************************
public enableInterstitial() {
  this.interstital.state = true;
}
//**********************************************************************************************************************
private isInterstitialEnbaled() : boolean {
  return this.interstital.state == true;
}
//**********************************************************************************************************************
private checkInterstitalTimeExecution(){
  let date : Date = this.banner.lastExecution;
  if(date  == null){
    this.banner.lastExecution = new Date();
    return false;
  }
  // get current date
  let current : Date = new Date();
  let date2 = new Date(date.getTime());
  date2.setSeconds(date2.getSeconds() + this.interstital.interval);
  if(date2.getTime() <= current.getTime() ){
    this.banner.lastExecution = new Date();
    return true;
  }
  else{
    return false;
  }
}
//**********************************************************************************************************************
}
