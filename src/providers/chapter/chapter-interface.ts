import {Chapter} from "../../classes/chapter";

export abstract class IChapterService {
  abstract getAllChapter();
  abstract setChapterNavParam(chapter : Chapter);
  abstract getChapterNavParam() : Chapter;
  abstract getChaptersFromResultSet(ResultSet : any);
}
