import { Injectable } from '@angular/core';
import {IChapterService} from "./chapter-interface";
import {Chapter} from "../../classes/chapter";
import {DatabaseProvider} from "../database/database-provider";
import {SQLiteObject} from "@ionic-native/sqlite";
import {ITutorialService} from "../tutorial/tutorial-interface";

/*
  Generated class for the StaticChapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StaticChapterProvider extends IChapterService{
  private chapterNavParam  : Chapter = null;
  setChapterNavParam(chapter: Chapter) {
    this.chapterNavParam = chapter;
  }

  getChapterNavParam(): Chapter {
    return this.chapterNavParam;
  }

  private db : SQLiteObject;
  getChaptersFromResultSet(resultSet: any) {
    if (resultSet != null && resultSet.rows.length > 0) {
      this.chapters = [];
      let c : Chapter;
      for (let i = 0; i < resultSet.rows.length; i++) {
        c = new Chapter();
        c.id = resultSet.rows.item(i).id;
        c.number = resultSet.rows.item(i).number;
        c.title = resultSet.rows.item(i).title;
        c.description = resultSet.rows.item(i).description;
        this.chapters.push(c);
      }
      return this.chapters;
    }
  }

//**********************************************************************************************************************
  private chapters:Chapter[] = null;
//**********************************************************************************************************************
  constructor(public databaseProvider : DatabaseProvider,public  tutorialService : ITutorialService) {
    super();
    this.chapters = [];
  }
//**********************************************************************************************************************
  async getAllChapter() : Promise<Chapter []>{
    this.db = await this.databaseProvider.openDataBase();
    let resultSet : any = await this.db.executeSql('SELECT * FROM  CHAPTER ORDER BY NUMBER;',[]);
        this.chapters = this.getChaptersFromResultSet(resultSet);
        this.chapters.forEach(async(chapter : Chapter) => {
          await this.tutorialService.setTutotialsOfChapter(chapter);
        });
        return Promise.resolve(this.chapters);
  }
//**********************************************************************************************************************
}
