import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database-provider";

/*
  Generated class for the ChapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChapterProvider {

  constructor(public databaseProvider : DatabaseProvider) {
    console.log('Hello ChapterProvider Provider');
  }

}
