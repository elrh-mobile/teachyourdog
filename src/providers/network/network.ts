import { Network } from '@ionic-native/Network';
import { AlertController, Platform} from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { App } from 'ionic-angular/components/app/app';

/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkProvider {
//*********************************************************************************************************************
  private disconnectSubscription : Subscription;
  private connectSubscription : Subscription;
//*********************************************************************************************************************
  constructor(
              private network: Network,
              private alertCtrl: AlertController,
              private platform : Platform,
              private app: App
            )
  {
    platform.ready().then(() => {
      console.log('Hello NetworkProvider Provider');
      this.listenToConnect();
      this.listenToDisconnect();
    });
  }
//*********************************************************************************************************************
private listenToConnect(){
    this.platform.ready().then(()=> {
      this.connectSubscription = this.network.onConnect().subscribe(() => {
        console.log('network connected!');
        // We just got a connection but we need to wait briefly
        // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        setTimeout(() => {
          this.isConnected();
        }, 3000);
      });
    });
}
//*********************************************************************************************************************
private listenToDisconnect(){
  this.platform.ready().then(()=> {
    this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.isConnected();
      console.log('network was disconnected :-(');
    });
  });
}
//*********************************************************************************************************************
public isConnected() : boolean {
  if(this.network == null)
    return false;
  console.log(this.network.type);
  if( this.network.type == null)
    return false;
  if(this.network.type !== 'none'){
    console.log('APP IS CONNECTED TO INTERNET this.network.type ===> ');
    console.log(this.network.type);
    return true;
  }
  return false;
}
//*********************************************************************************************************************
public presentNoNetworkAlert(page : any = null) {
  let alert = this.alertCtrl.create({
    title: 'No Network !',
    subTitle: 'Please check your network connection',
    buttons:  [{
      text: 'Ok',
      handler: () => {
        // get dismiss promise
        let navTransition = alert.dismiss();
        // navigate
        navTransition.then(() => {
          if( page != null){
            this.app.getRootNav().setRoot(page);
          }
        });
        return false;
      }
    }],
    cssClass : 'tyd-alert'
  });
  alert.present();
}
//*********************************************************************************************************************
}
