/**
 * Script de creation des tables
 * @type {[string , string , string , string , string , string , string , string]}
 */
export const CREATE_TABLES_ARRAY : string [] =
  [
    'CREATE TABLE IF NOT EXISTS `chapter` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
    '`number` INTEGER NOT NULL,'+
    '`title` TEXT,`description` TEXT NOT NULL);'
    ,
    'CREATE TABLE IF NOT EXISTS `tutorial` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
    '`number` INTEGER NOT NULL,'+
    '`title` TEXT,`id_chapter` integer NOT NULL,FOREIGN KEY(`id_chapter`) REFERENCES `chapter`(`id`));'
    ,
    'CREATE TABLE IF NOT EXISTS `teach_it` ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `id_tutorial` ' +
    'integer NOT NULL, FOREIGN KEY(`id_tutorial`) REFERENCES `tutorial`(`id`) );'
    ,
    'CREATE TABLE IF NOT EXISTS `IMAGE_STEP` (' +
    '`number` INTEGER NOT NULL,' +
    '`description` TEXT NOT NULL,' +
    '`id_teach_it` integer NOT NULL,' +
    'FOREIGN KEY(`id_teach_it`) REFERENCES `teach_it`(`id`),' +
    'PRIMARY KEY(`number`,`id_teach_it`)' +
    ');'
    ,
    'CREATE TABLE IF NOT EXISTS `details` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
    ' `id_tutorial` integer NOT NULL, ' +
    ' `description` TEXT, ' +
    ' FOREIGN KEY(`id_tutorial`) REFERENCES `tutorial`(`id`) ' +
    ');'
    ,
    'CREATE TABLE IF NOT EXISTS `STEP` ( ' +
    ' `number` INTEGER NOT NULL, ' +
    ' `description` TEXT NOT NULL, ' +
    ' `id_details` integer NOT NULL, ' +
    ' FOREIGN KEY(`id_details`) REFERENCES `details`(`id`), ' +
    ' PRIMARY KEY(`number`,`id_details`) ' +
    ');'
    ,
    'CREATE TABLE IF NOT EXISTS `Troubleshooting` ( ' +
    ' `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
    ' `description` TEXT, ' +
    ' `id_tutorial` integer NOT NULL, ' +
    ' FOREIGN KEY(`id_tutorial`) REFERENCES `tutorial`(`id`) ' +
    ');'
    ,
    'CREATE TABLE IF NOT EXISTS `question_response` ( ' +
    ' `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
    ' `question` TEXT NOT NULL, ' +
    ' `response` TEXT NOT NULL, ' +
    ' `troubleshooting_id` integer NOT NULL, ' +
    ' FOREIGN KEY(`troubleshooting_id`) REFERENCES `Troubleshooting`(`id`) ' +
    ');'
  ];
