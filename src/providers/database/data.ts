export const TeachYourDogData = {
  chapters : [
    {
      title : 'GROUNDWORK',
      number : 1,
      description :
      '“Obedience” is a word often misinterpreted in dog training to suggest the imposition of a ' +
      'dominating control over our dog. But let’s get past the word and think of basic obedience skills as the ' +
      'groundwork upon which a successful living arrangement between dog and owner is achieved. The sit, down, ' +
      'come, and stay behaviors are marks of a civilized and well-behaved dog',
      tutorials : [
        // sit
        {
          title : 'Sit',
          teachIt : {
            imgSteps : [
               {
                  description : 'Hold a treat over your dog’s head.',
              },
              {
                  description : 'Move it straight back.',
              },
              {
                  description : 'Press his haunches while pulling up on the leash.',
              },
            ]
          }
          ,
          details : {
            description : 'Your dog sits squarely on his hindquarters and remains there until released.',
            steps : [
              {
                  description : 'Stand or kneel in front of your dog, holding a treat in your hand a little higher ' +
                              'than your dog’s head.',
              },
              {
                  description : 'Slowly move the treat straight back over your dog’s head. This should cause ' +
                                'his nose to point up and his rear to drop. If his rear does not drop, keep ' +
                                'moving the treat straight backward toward his tail. The instant his rear touches ' +
                                'the floor, release the treat and mark the behavior by saying “good sit! '
              },
              {
                  description : 'If your dog is not responding to the food lure, use your index finger and thumb ' +
                                'to put pressure on either side of his haunches, just forward of his hip bones. ' +
                                'Pull up on his leash at the same time to rock him back into a sit. Praise and ' +
                                'reward him while he is sitting.'
              },
              {
                description : 'Once your dog is consistently sitting, wait a few seconds before rewarding.' +
                              'Remember to only reward while your dog is in the correct position—sitting.',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                  question : 'My Dog Jumps At My Hand With The Treat',
                  response : 'Hold the treat lower, so that he can reach it while standing.'
              },
              {
                  question : 'MY DOG SITS, BUT KEEPS GETTING UP',
                  response : 'In a gentle but firm manner, keep placing your dog back in a sit. Once ' +
                            'he has learned the behavior, he should not break his sit until released.'
              },
            ]
          }
        },
        // down
        {
          title : 'Down',
          teachIt : {
            imgSteps : [
              {
                description : 'Hold a treat to your dog’s nose',
              },
              {
                description : ' Lower the treat to the floor',
              },
              {
                description : 'Slide the treat toward or away from him.',
              },
              {
                description : 'Release the treat once your dog lies down',
              },
              {
                description : 'Press downward and to the side',
              },
            ]
          }
          ,
          details : {
            description : 'Your dog drops to rest on either his chest and belly or askew on his hip.' +
            ' This vital command could help avert dangerous situations such as unsafe road crossings. ',
            steps : [
              {
                description : ' With your dog sitting facing you, hold a treat to his nose and lower ' +
                'it slowly to the floor. ',
              },
              {
                description : ' If you’re lucky, your dog will follow the treat with his' +
                ' nose and lie down, at which time you can release the treat and praise him.' +
                ' Remember to only release the treat while your dog is in the correct position—lying' +
                ' down. If your dog slouches instead of lying down, slide the treat slowly toward' +
                ' him on the floor between his front paws or away from him. It may take a little' +
                ' time but your dog should eventually lie down. '
              },
              {
                description : ' If your dog is not responding to the food lure, put slight pressure on his' +
                'shoulder blade, pushing down and to the side. Praise your dog when he drops to the floor. ' +
                'It is always preferable to coax the dog to position himself without your physical manipulation. '
              },
              {
                description : 'Once your dog is consistently lying down, gradually delay the release of the' +
                ' treat. With your dog lying down, say “wait, wait” and then “good” and release ' +
                'the treat. Varying the time before treating will keep your dog focused. The dog ' +
                'should not move from the down position until you have given your release word, “OK!” ',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY DOG IS RESISTANT TO THIS BEHAVIOR ',
                response : 'Your dog lying down before you is interpreted as subservience to you. ' +
                'Evaluate your status as pack leader. '
              },
              {
                question : 'MY DOG DOESN’T STAY DOWN',
                response : 'If he stands up, don’t reward him, and put him back down. Standing on ' +
                'his leash will cause him to self-correct if he tries to stand up. '
              },
              {
                question : 'MY DOG DOWNS IN ONE ROOM, BUT NOT ANOTHER ',
                response : 'Pay attention to the ground surface. Short-coated dogs will often resist ' +
                'downing on hard floor. Try a carpet or towel. '
              },
            ]
          }
        },
        // stay
        {
          title : 'Stay',
          teachIt : {
            imgSteps : [
              {
                description : 'Command your dog to “stay.”',
              },
              {
                description : 'Move a short distance away',
              },
              {
                description : 'Return	him	to the	original spot if he breaks.',
              },
            ]
          }
          ,
          details : {
            description : 'When in a stay, your dog holds his current position until released.',
            steps : [
              {
                description : ' Start with your dog sitting or lying down, as he is less likely ' +
                              'to move from these positions. Use a leash to guarantee control.' +
                ' Stand directly in front of him and in a serious tone, say “stay,” holding your' +
                ' palm flat, almost touching his nose. ',
              },
              {
                description : ' Move a short distance away, keeping eye contact with your dog, and return' +
                ' to him. Praise him with “good stay” and give him a treat. Be sure to give the ' +
                'praise and treat while your dog remains in the seated and staying position. '
              },
              {
                description : 'If your dog moves from his stay before you have released him, gently but' +
                ' firmly put him back in the spot where he was originally told to stay. '
              },
              {
                description : 'Gradually increase the time you ask your dog to stay, as well as the' +
                ' distance between yourselves. You want your dog to be successful so if he is' +
                ' breaking his stays, go back to a time and distance he is able to achieve. ',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY DOG KEEPS GETTING UP',
                response : 'Use very little verbal communication when teaching this skill. Talking ' +
                'evokes action, and you want inaction. Solid body language will convey your seriousness. '
              },
              {
                question : 'MY DOG SEEMS TO BREAK HIS STAY A SECOND BEFORE I RELEASE HIM.',
                response : 'Do not show him the treat until you give it to him, as it may pull him ' +
                'forward. Vary your pattern; sometimes return to him and leave him again without rewarding.'
              },
            ]
          }
        },
        // come
        {
          title : 'Come',
          teachIt : {
            imgSteps : [
              {
                description : 'Reel	your dog in to you.',
              },
              {
                description : 'Move it straight back.',
              },
              {
                description : 'Train off-lead in a fenced area.',
              },
            ]
          }
          ,
          details : {
            description : 'Upon your command, your dog comes immediately to you. In competition, this ' +
                          'command ends with your dog sitting in front of you. In order for this command ' +
                          'to be consistently obeyed, your status as pack leader needs to be definite' +
                          '. Always reward your dog for obeying your “come” command, whether it be with' +
                          ' praise or a treat. Not obeying this command, however, should be viewed as a ' +
                          'major infraction and should end with you physically bringing your dog to the' +
                          ' spot from where you originally called him. ',
            steps : [
              {
                description : 'With your dog on a 6’ (1.8 m) lead, command him to “come” and reel him quickly in ' +
                              'to you, where he will be praised. Your command should sound happy, but firm. Give' +
                              ' the command only once.'
              },
              {
                description : 'As your dog improves, graduate to a longer lead.'
              },
              {
                description : 'When you are ready to practice off-lead, do it in a fenced area.' +
                ' Let your dog drag a leash. If he does not obey your first command,' +
                ' go to him and firmly lead him back to the spot where you gave the command.' +
                ' Do not give a reward if the dog does not perform the command on his own,' +
                ' the first time you call. Put the long lead back on him and require him to' +
                ' do five successful “comes” before attempting off-lead again. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'ONCE OFF LEAD MY DOG RUNS OFF! ',
                response : 'Do not chase your dog, as that will only encourage him.' +
                           ' Stand your ground and demand that he come. Dogs respond to a leader'
              },
              {
                question : 'DO I HAVE TO ENFORCE THIS COMMAND EVERY TIME I USE IT? ',
                response : 'Yes. If you are not in a position to enforce it, don’t give the command. ' +
                          'Instead just call your dog’s name or use “c’mon boy!” '
              },
            ]
          }
        },
      ]
    },

    // empty chapter
    {
      title : 'Traditional	Favorites',
      number : 2,
      description :'Fetch,	shake,	speak,	and	play	dead…	these	useful,	useless,	and	always charming	tricks	have	been	around	since	cavemen	first	shared	their	bones with	wolves.	Regardless	of	a	lack	of	titles	after	his	name,	a	dog	who	falls	to the	ground	on	the	command	of	“bang”	or	offers	a	polite	paw	to	his	guests will	be	top	dog	among	your	friends!	These	tricks	are	expected	of	dogs	and	it is	your	task,	possibly	even	your	duty,	to	teach	them	to	your	clever	canine.',
      tutorials : [
        // tutorial 1
        {
          title : 'Shake Hands—Left	and	Right',
          teachIt : {
            imgSteps : [
               {
                  description : 'Hide	a	treat	in	your	right	hand,	low	to	the	ground.',
              },
              {
                  description : 'As	your	dog	improves,	raise	your	hand.',
              },
              {
                  description : 'Stand	up	and	cue	your	dog.',
              },
              {
                description : 'Hold	his	paw	while	you	reward.',
              },
            ]
          }
          ,
          details : {
            description : 'When	shaking	hands,	your	polite	pooch	raises	his	paw	to	chest	height,	allowing guests	to	shake	his	paw.	This	skill	is	taught	for	both	paws. ',
            steps : [
              {
                  description : 'With	your	dog	sitting	before	you,	hide	a	treat	in	your	right	hand,	low	to	the ground.	Encourage	your	dog	to	paw	at	it	by	saying	“get	it”	and	“shake.” Reward	your	dog	with	the	treat	the	moment	his	left	paw	comes	off	the	ground. ',
              },
              {
                  description : 'Gradually	raise	the	height	of	your	hand,	upping	the	ante,	until	he	is	lifting	his paw	to	chest	height.'
              },
              {
                  description : 'Transition	to	using	the	hand	signal.	Stand	up	and	hold	the	treat	in	your	left hand,	behind	your	back,	and	extend	your	right	hand	while	cuing	“shake.” When	your	dog	paws	your	extended	hand,	support	his	paw	in	the	air	while you	reward	him	with	the	treat	from	behind	your	back. '
              },
              {
                description : '	Repeat	these	steps	on	the	opposite	side	to	teach	“paw.”',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                  question : 'INSTEAD	OF	PAWING	AT	MY	HAND,	MY	DOG	NOSES	IT ',
                  response : 'Bop	his	nose	a	little	to	discourage	this.	He	may	try	barking,	nuzzling,	or doing	nothing.	Be	patient,	and	keep	encouraging	him.	If	he	is	not	lifting his	paw	on	his	own,	tap	or	barely	lift	it	for	him	and	then	reward.'
              },
            ]
          }
        },
        // tutorial 2
        {
          title : 'Fetch',
          teachIt : {
            imgSteps : [
              {
                description : 'Make	a	slit	in	a	tennis	ball	and	drop	a	treat	inside',
              },
              {
                description : 'Toss	the	ball	playfully.',
              },
              {
                description : 'Squeeze	the	ball	to	release	the	treat',
              },
            ]
          }
          ,
          details : {
            description : 'In	fetch,	your	dog	is	directed	to	retrieve	a	specified	object.',
            steps : [
              {
                description : '	Use	a	box	cutter	to	make	a	1”	(2.5	cm)	slit	in	a	tennis	ball.	Show	your	dog	as you	drop	a	treat	inside	the	ball. ',
              },
              {
                description : 'Toss	the	ball	playfully	and	encourage	the	dog	to	bring	it	back	to	you	by	patting your	legs,	acting	excited,	or	running	from	him. '
              },
              {
                description : 'Take	the	ball	from	your	dog	and	squeeze	it	to	release	the	treat	for	him.	As	he	is unable	to	get	the	treat	himself,	he	will	learn	to	bring	it	to	you	for	his	reward. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	HAS	NO	INTEREST	IN	CHASING	THE	BALL',
                response : 'Motivate	your	dog	by	acting	excited	and	chasing	the	ball	yourself.	Bat it	around	or	bounce	it	off	walls.	Make	it	a	competition	and	race	him	for it. '
              },
            ]
          }
        },
        // tutorial 3
        {
          title : 'TAKE	IT',
          teachIt : {
            imgSteps : [
              {
                description : 'Hand	your	dog	a	favorite	toy.',
              },
              {
                description : 'Trade	him	a	treat	for	the	toy.',
              },
            ]
          }
          ,
          details : {
            description : 'Take it is when he takes an object within reach into his mouth.',
            steps : [
              {
                description : 'Select	a	toy	that	your	dog	enjoys	and	playfully	hand	it	to	him	while	giving	the verbal	cue. ',
              },
              {
                description : '	Have	him	hold	it	only	a	few	seconds	before	removing	it	from	his	mouth	and trading	him	a	treat	for	it.	As	your	dog	improves,	extend	the	time	he	holds	the object	before	treating.	Only	treat	if	you	remove	the	toy	from	your	dog’s mouth,	not	if	he	drops	it	on	his	own. '
              },
              {
                description : 'Be	creative!	Have	your	dog	hold	a	flag	as	he	circles	the	field	or	have	him	carry a	charming	“feed	me”	sign.	A	dog	holding	a	pipe	is	always	good	for	a	laugh, and	a	posh	pooch	carrying	a	basket	of	cocktail	napkins	is	sure	to	impress! '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	GETS	THE	BALL	AND	RUNS	OFF	WITH	IT ',
                response : 'Never	chase	your	dog	when	he	is	playing	keep-away.	Lure	him	back with	a	treat,	or	run	away	from	him	to	encourage	him	to	chase	you.	Have a	second	ball	to	get	his	attention'
              },
            ]
          }
        },
        // tutorial 4
        {
          title : 'Drop	It',
          teachIt : {
            imgSteps : [
              {
                description : 'Point	to	the	ground	and	command	“drop	it.”',
              },
            ]
          }
          ,
          details : {
            description : 'On	the	drop	it	cue,	your	dog	releases	the	object	from	his	mouth,	dropping	it onto	the	ground.',
            steps : [
              {
                description : 'Is	your	dog	food	or	toy	motivated?	Point	to	the	ground	and	command	your	dog to	“drop	it.”	Do	not	move	from	your	location,	and	keep	repeating	the command.	It	may	take	several	minutes,	but	when	your	dog	finally	drops	the toy,	reward	him	with	food	or	by	throwing	his	toy. '
              }
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	WILL	NOT	RELEASE	THE	TOY',
                response : 'Try	using	a	less	desirable	toy	and	rewarding	him	with	a	highly	desired toy	when	he	obeys'
              },
            ]
          }
        },
        // tutorial 5
        {
          title : 'GIVE',
          teachIt : {
            imgSteps : [
              {
                description : 'Trade a treat for your dog’s toy.',
              },
            ]
          }
          ,
          details : {
            description : 'Give is released to your hand.',
            steps : [
              {
                description : 'While your dog has a toy in his mouth, tell him to “give” and offer him a treat in exchange for the toy. He will have to release the toy to eat the treat, at which time you can praise him.'
              },
              {
                description : 'Give your dog his toy back, so he understands that relinquishing it to you does not mean that it will be taken away.'
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'SHOULD I FORCE AN OBJECT AWAY FROM HIM?',
                response : 'No, as this could result in a dog bite, intentional or not. A better way to get a dog to release his grip is to pull upward on the skin on the side of his rib cage.'
              },
            ]
          }
        },
        // tutorial 6
        {
          title : 'Speak',
          teachIt : {
            imgSteps : [
              {
                description : 'Ring the doorbell.',
              },
              {
                description : 'Try to elicit the behavior with only the verbal cue.',
              },
              {
                description : 'Change locations and cue your dog.',
              },
            ]
          }
          ,
          details : {
            description : 'Y our	dog	barks	on	cue.	If	your	dog	is	barking	up	the	wrong	tree	…	then	this	is the	trick	for	him!',
            steps : [
              {
                description : 'Observe	what	causes	your	dog	to	bark—a	doorbell	or	knock,	the	postman,	the sight	of	you	with	his	leash—and	use	that	stimulus	to	teach	this	trick.	Because most	dogs	bark	at	the	sound	of	a	doorbell,	we’ll	use	that	as	an	example.	Stand at	your	front	door,	with	the	door	open	so	your	dog	will	be	able	to	hear	the	bell. Give	the	cue	“bark”	and	press	the	doorbell.	When	your	dog	barks, immediately	reward	him	and	reinforce	the	cue	with	“good	bark.”	Repeat	this about	six	times. '
              },
              {
                description : '	Continuing	in	the	same	session,	give	the	cue	but	don’t	ring	the	bell.	You	may have	to	cue	several	times	to	get	a	bark.	If	your	dog	is	not	barking,	return	to	the previous	step. '
              },
              {
                description : 'Try	this	trick	in	a	different	room.	Strangely	enough,	this	can	be	a	difficult transition	for	your	dog.	If	at	any	point	your	dog	is	repeatedly	unsuccessful, return	to	the	previous	step. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'I’VE	CREATED	A	BARKING	MONSTER!',
                response : 'Never	reward	your	dog	for	a	bark	unless	you	asked	for	this	behavior. Otherwise	he’ll	speak	up	anytime	he	wants	something! '
              },
              {
                question : 'I	CAN’T	FIND	A	STIMULUS	TO	MAKE	MY	DOG	BARK. ',
                response : 'Dogs	will	often	bark	out	of	frustration.	Tease	him	with	a	treat:	“Do	you want	it?	Speak	for	it!”'
              },
            ]
          }
        },
       // tutorial 7
       {
        title : 'Play	Dead',
        teachIt : {
          imgSteps : [
            {
              description : '	Put	your	dog	in	a	down,	facing	you.',
            },
            {
              description : 'Lure	him	onto	his	side,	as	in	a	rollover.',
            },
            {
              description : '	Continue	to	lure	him	onto	his	back	and	steady	him	there.',
            },
            {
              description : 'Practice	until	your	dog	can	play	dead	on	cue!',
            },
          ]
        }
        ,
        details : {
          description : 'When	playing	dead,	your	dog	rolls	onto	his	back	with	his	legs	in	the	air.	He remains	“dead”	until	you	cue	his	miraculous	recovery.	Stick	’em	up	or	you’re	a dead	dog! ',
          steps : [
            {
              description : '	Teach	this	trick	after	your	dog	has	had	some	exercise	and	is	ready	to	rest.	Put your	dog	in	a	down and	kneel	in	front	of	him.	Hold	a	treat	to	the side	of	his	head	and	move	it	toward	his	shoulder	blade,	as	you	did	when teaching	roll	over .	Your	dog	should	fall	to	his	side. '
            },
            {
              description : 'Continue	to	roll	him	to	his	back	by	guiding	his	midsection.	Praise	him	and	give him	a	belly	scratch	while	he	is	on	his	back.	Reinforce	the	verbal	cue	by	saying “good	bang.” '
            },
            {
              description : 'As	your	dog	improves,	try	to	lure	him	into	position	with	the	treat	only,	without touching	him.	If	he	is	likely	to	roll	completely	over	instead	of	stopping	half way,	stop	him	with	your	hand	on	his	chest,	and	then	slowly	release	your	grip so	that	he	holds	the	position	on	his	own. '
            },
            {
              description : 'Practice	this	skill	until	you	are	able	to	elicit	the	behavior	with	the	“bang!”	cue and	hand	signal.	Your	dog	should	stay	in	this	position	until	he	is	released	with “OK”	or	“you	are	healed!”	or	some	other	release	word. '
            },
          ]
        }
        ,
        troublshooting : {
          questionsResponses : [
            {
              question : 'DEAD	DOGS	SHOULDN’T	HAVE	WAGGING	TAILS! ',
              response : 'Try	lowering	your	voice	to	a	more	commanding	tone	to	stop	the wagging.	Or	don’t	worry	about	it	…	it’s	sure	to	get	a	giggle! '
            },
            {
              question : 'MY	DOG	DOES	A	SLOW	AGONIZING	DEATH	THAT	REQUIRES SEVERAL	BULLETS	AND	A	FEW	CIRCLES ',
              response : 'Improvise	with	“darn,	missed	him!	Will	you	die	already!	Talk about	a	scene	stealer!”'
            },
          ]
        }
      },
      ]
    },





    // DOGS LOVE
    {
      title : 'Love	Me,	Love	My	Dog',
      number : 3,
      description :'Puppy	dog	eyes	can	melt	the	hardest	of	hearts	and	unravel	the	strongest	of wills	because	we,	after	all,	love	our	dogs.	Obedience	trainers	and	dog behaviorists	may	scorn	as	we	sleep	with	furry	foot	warmers	or	perch	our pooch	on	our	lap	and	(heaven	forbid!)	kiss	him	on	the	lips.	But	rules	are made	to	be	broken,	and	we	promise	…	we	won’t	tell! “Quo	me	amat,	amat	et	canem	meam.”	Love	me,	love	my	dog.	This	Latin proverb	quoted	to	Saint	Bernard	has	been	repeated	in	almost	every	language throughout	the	centuries. Go	ahead	and	celebrate	the	close	bond	between	you	and	your	dog	with	the intimate	tricks	in	this	chapter.	These	expressive	behaviors	will	endear	him	to all!',
      tutorials : [
        // tutorial 1
        {
          title : 'Kisses',
          teachIt : {
            imgSteps : [
               {
                  description : 'Let	your	dog	to	take	a	treat	from	your	teeth.',
              },
              {
                  description : 'Put	peanut	butter	on	your	cheek.',
              },
              {
                  description : 'Point	to	your	lips	for	a	kiss.',
              },
            ]
          }
          ,
          details : {
            description : 'Your	dog	licks	or	noses	the	lips	or	cheek	of	you	or	another	person',
            steps : [
              {
                  description : '	Sit	at	“doggy-level.”	Give	the	verbal	cue	and	place	a	treat	between	your	teeth as	you	lean	forward.	Allow	your	dog	to	take	the	treat,	and	praise	him	with “good	kisses!” ',
              },
              {
                  description : 'If	you	do	not	wish	your	dog	to	kiss	you	on	your	lips	(although	I	can’t	imagine why!),	put	some	peanut	butter	on	your	cheek,	point	to	it	while	saying	“kisses,” and	let	him	lick	it	off. '
              },
              {
                  description : 'With	a	treat	held	behind	your	back,	point	to	your	lips	or	cheek	and	tell	your dog	“kisses!”	When	he	licks	or	noses	you,	mark	the	instant	with	“good!”	and reward	him	with	the	treat. '
              },
              {
                description : 'Now	try	it	with	someone	else.	Have	a	helper	apply	some	peanut	butter	to	their cheek.	Point	to	it	and	cue	your	dog.	When	he	licks	your	helper’s	cheek,	tell him	“good,”	and	reward	him.	Step	back	and	send	your	dog	a	farther	distance to	give	kisses.	Phase	out	the	peanut	butter	and	have	your	dog	return	to	you	for his	treat.',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                  question : 'MY	DOG	BITES	MY	LIP ',
                  response : 'Address	this	issue	separately	by	telling	your	dog	“easy”	as	you	allow him	to	take	treats.	Bop	him	on	the	nose	if	he	bites,	and	say	“ouch!” '
              },
              {
                  question : 'MY	DOG	IS	SCARED	NEAR	MY	FACE ',
                  response : 'Your	dog	is	putting	himself	in	a	submissive	position	by	coming	close	to your	mouth	(which	in	dog	culture	could	lead	to	a	bite).	This	trick requires	trust.	Try	holding	the	treat	several	inches	from	your	mouth,	and as	he	reaches	for	it,	bring	it	closer	to	your	face.'
              },
            ]
          }
        },
        // tutorial 2
        {
          title : 'Paws	on	My	Arm',
          teachIt : {
            imgSteps : [
              {
                description : 'Lure	your	dog	onto	your	arm	and	allow	him	to	nibble	a	treat.',
              },
              {
                description : 'Repeat	this	exercise	while	standing.',
              }
            ]
          }
          ,
          details : {
            description : 'If	your	pet	peeve	is	a	pet	that	jumps	on	guests,	teach	him	to	welcome	visitors with	paws	on	their	arm	to	give	him	a	safe	and	manageable	way	to	show	his enthusiasm. ',
            steps : [
              {
                description : '	Sit	on	the	floor	with	your	dog	on	your	left.	Raise	your	left	arm	in	front	of	him and	lure	his	head	upward	with	a	treat	in	your	right	hand.	Your	dog	will probably	place	one	or	both	paws	on	your	forearm,	in	an	effort	to	reach	the treat,	but	if	he	doesn’t	you	can	coax	his	paws	onto	your	arm	with	your	hands. Once	your	dog	is	in	the	correct	position,	with	his	paws	resting	on	your	arm, allow	him	to	nibble	treats	from	your	hand. ',
              },
              {
                description : 'Try	this	exercise	while	standing	up.	Use	the	verbal	cue	and	hand	signal.	You may	wish	to	hold	the	treat	in	your	mouth	until	you	are	ready	to	give	it	to	your dog	to	keep	him	from	becoming	distracted	by	it	(hot	dogs	or	cheese	work well).'
              }
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	PUTS	ONLY	ONE	PAW	ON	MY	ARM ',
                response : 'To	start,	you	may	have	to	use	your	hand	to	guide	his	second	paw	up. '
              },
              {
                question : 'MY	DOG	IS	STILL	JUMPING	ON	PEOPLE! ',
                response : 'This	hand	signal	will	become	an	invitation	for	your	dog	to	raise	himself to	your	arm.	Be	clear	with	your	rules—without	an	invitation,	your	dog should	be	reprimanded	for	jumping	on	people	(assuming	that	is	your rule).'
              }
            ]
          }
        },
        // tutorial 3
        {
          title : 'Head	Down',
          teachIt : {
            imgSteps : [
              {
                description : 'A	combination	of	a	food	lure	and	your	hand	pressure	will	guide	your	dog’s head	down',
              },
              {
                description : 'Slide	the	treat	to	your	dog	while	keeping	him	in	the	proper	position.',
              },
              {
                description : '	Use	your	hand	signal	to	focus	your	dog’s	attention	downward.',
              },
            ]
          }
          ,
          details : {
            description : 'From	a	down	position,	your	dog	lowers	his	head	to	rest	upon	the	floor.	This	is	a common	trick	for	movie	dogs.	“Aww,	the	doggy	looks	sad!” ',
            steps : [
              {
                description : '	Kneel	to	the	side	of	your	dog	as	he	rests	in	a	down	position.	Hold	a	treat	on	the floor,	out	of	reach	in	front	of	him.	Cue	“head	down”	while	using	your	other hand	to	gently	push	his	head	to	the	floor	using	pressure	points	behind	his	ears. ',
              },
              {
                description : 'Hold	him	for	a	few	seconds	with	his	chin	resting	on	the	floor	between	his paws,	then	praise	and	slide	your	treat	toward	him.	Allow	him	to	take	the	treat and	then	give	your	release	word	“OK”	and	release	his	head	so	he	can	chew.	If your	dog	is	very	resistant	to	your	physical	manipulation,	reward	the	instant	his chin	touches	the	floor,	so	as	not	to	cause	him	to	struggle. '
              },
              {
                description : 'Gradually	lighten	your	touch	on	his	head,	so	that	you	do	short	taps	rather	than constant	pressure.	Once	his	head	is	down,	instruct	him	to	“stay”	a	few	seconds before	rewarding.	Always	present	your	reward	on	the	floor,	so	your	dog	isn’t tempted	to	look	up	for	it. '
              }
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	RUNS	AWAY	WHEN	I	TRY	TO	TRAIN	THIS	TRICK ',
                response : 'Physically	manipulating	your	dog	is	a	slippery	slope.	He	may	think	you are	dominating	him	or	punishing	him	by	pushing	his	head	down. Progress	slowly	and	gently	with	this	trick,	and	only	practice	two	or three	times	per	session.	Praise	lavishly!'
              }
            ]
          }
        },
        // tutorial 4
        {
          title : 'Cover	Your	Eyes',
          teachIt : {
            imgSteps : [
              {
                description : 'Encourage	your	dog	to	swipe	at	a	sticky	note	on	his	face.',
              },
              {
                description : 'While	laying	down,	your	dog	will	poke	his	head	under	his	paw.',
              },
              {
                description : 'Just	tap	the	spot	on	his	head	instead	of	using	a	sticky	note.',
              },
              {
                description : 'Go	back	to	using	the	sticky	note,	but	this	time	in	the	sitting	position.',
              },
              {
                description : 'Stand	up	to	encourage	your	dog’s	head	higher.',
              },
              {
                description : 'Try	an	eye	cover	in	a	bow	position.',
              },
            ]
          }
          ,
          details : {
            description : 'Your	dog	hides	his	eyes	by	hooking	his	paw	over	his	muzzle. ',
            steps : [
              {
                description : 'Stick	a	sticky	note	or	piece	of	tape	to	your	dog’s	muzzle	and	encourage	him	to “cover,	get	it!”	One	swipe	at	his	face	should	be	enough	to	dislodge	the	paper. Praise	him	with	“good	cover!” '
              },
              {
                description : '	With	your	dog	laying	down,	stick	the	note	to	the	center	of	his	head,	just	above his	eyes.	He	will	have	a	harder	time	swatting	at	this	spot	and	will	eventually poke	his	head	under	one	wrist.	Perfect!	Be	ready	to	reward	him	in	the	spot where	his	head	pokes	under	his	paw. '
              },
              {
                description : '	Alternate	between	using	the	sticky	note	and	just	tapping	his	head	in	the	spot where	you	normally	stick	the	sticky	note.	Use	“stay”	to	get	your	dog	to	hold the	position	for	a	few	seconds. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	SHAKES	HIS	HEAD	INSTEAD	OF	PAWING	AT	THE STICKY	NOTE',
                response : 'Use	a	stronger	adhesive	tape	so	your	dog	can’t	merely	shake	it	off.	Cue him	to	shake	hands	to	give	him	the	idea	to	use	his	paw.	Stick the	note	in	different	places:	above	or	below	his	eye	or	on	top	of	his head. '
              },
              {
                question : 'MY	DOG	JUST	SITS	THERE	WITH	THE	PAPER	STUCK	TO	HIS NOSE! ',
                response : 'Your	dignified	dog	needs	to	be	encouraged	to	attack	the	object,	as	he would	a	bug	on	his	nose.	Touch	the	paper	to	make	him	aware	of	it	and use	your	voice	to	excite	him.'
              },
            ]
          }
        },
        // tutorial 4
        {
          title : 'Wave	Good-bye',
          teachIt : {
            imgSteps : [
              {
                description : 'Have	your	dog	shake	hands.',
              },
              {
                description : 'Extend	your	hand	higher	than	normal.',
              },
              {
                description : 'With	your	hand	farther	away,	your	dog	can	just	barely	reach	your	fingers.',
              },
              {
                description : 'Pull	your	hand	back	at	the	last	second	so	your	dog	paws	the	air.',
              },
              {
                description : 'Transition	to	the	hand	signal.',
              },
              {
                description : '“Bye,	bye!”',
              },
            ]
          }
          ,
          details : {
            description : 'Your	dog	waves	his	paw	high	in	the	air.',
            steps : [
              {
                description : 'With	your	dog	sitting,	face	him	and	have	him	shake	hands.'
              },
              {
                description : 'Say	“shake,	bye-bye”	and	extend	your	hand	a	little	higher	than	you	normally would	to	shake	hands.	Your	dog	won’t	be	able	to	hold	his	paw	that	high,	so	his motion	will	look	like	he	is	pawing	at	your	hand. '
              },
              {
                description : 'Draw	your	hand	slightly	away	from	your	dog,	so	he	can	just	barely	reach	your fingers'
              },
              {
                description : '	Pull	your	hand	back	at	the	last	second	so	he	is	not	touching	it	at	all,	but	merely pawing	the	air.	Be	sure	to	praise	him,	so	he	understands	that	the	desired behavior	is	the	waving	motion,	as	opposed	to	the	actual	touch. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'AS	I	MOVE	AWAY	FROM	MY	DOG,	HE	KEEPS	MOVING	TOWARD ME,	TRYING	TO	TOUCH	MY	HAND ',
                response : 'Stand	a	few	feet	away	from	your	dog	with	your	hand	outstretched toward	him	as	you	cue	him.	At	the	last	second,	pull	your	hand	away	so that	he	is	pawing	the	air.	Reward	this! '
              },
              {
                question : 'MY	DOG	STANDS	UP',
                response : 'Put	him	back	in	a	sit	before	continuing	to	train.	He	will	have	higher extension	from	a	sitting	position.'
              },
            ]
          }
        },
      ]
    },




    // empty chapter
    {
      title : 'Let’s	Play	a	Game!',
      number : 5,
      description :'Playing	a	game	with	your	dog	builds	communication	skills	as	well	as establishes	rules	that	will	penetrate	throughout	your	relationship.	Think	of yourself	as	a	coach	while	teaching	these	tricks.	Use	energy	and	motivation	in equal	parts	with	discipline	and	authority.	The	game	should	be	a	reward	in itself,	and	your	dog	will	be	required	to	follow	rules	in	order	to	get	this reward.	Be	fair,	be	honest,	and	be	patient.	Every	big-league	star	started	in	the pee-wees	and	your	dog	will	start	there,	too. Let’s	go	outside	and	play !',
      tutorials : [
        // tutorial 1
        {
          title : 'Soccer',
          teachIt : {
            imgSteps : [
              {
                  description : 'Fill	a	treat	ball	with	kibble.',
              },
              {
                  description : 'Let	your	dog	play	with	it	on	his	own.',
              },
              {
                  description : 'Using	an	empty	treat	ball,	toss	the	treat	to	your	dog	when	he	rolls	the	ball.',
              },
              {
                  description : 'Transition	to	rewarding	from	your	hand.',
              },
              {
                  description : 'Reward	a	short	roll	with	a	soccer	ball.',
              },
              {
                  description : 'Set	a	distinct	goal	line	for	your	dog	to	cross.',
              },
            ]
          }
          ,
          details : {
            description : 'Sports	fans	are	sure	to	get	a	kick	out	of	your	superstar	dog	as	he	goes	for	the goal	by	rolling	a	soccer	ball	into	a	net. ',
            steps : [
              {
                  description : '	A	treat	ball	toy	sold	in	pet	stores	is	a	hollow	plastic	ball	with	a	hole	that,	when rolled,	randomly	releases	treats.	Fill	it	with	kibble	or	goldfish	crackers	and allow	your	dog	a	few	days	to	play	with	it	on	his	own.	It	will	likely	become	a favorite	toy. ',
              },
              {
                  description : 'Point	to	an	empty	treat	ball	and	tell	your	dog	“soccer!”	When	he	rolls	the	ball	a few	feet,	toss	a	treat	near	the	ball	for	him	to	find. '
              },
              {
                  description : 'Gradually	require	longer	roll	times	before	rewarding,	and	switch	to	rewarding from	your	hand	instead	of	tossing	the	treat.'
              },
              {
                description : 'Substitute	a	soccer	ball,	giving	the	same	verbal	cue	and	rewarding	for	a	short roll.	Gradually	build	up	the	distance.',
              },
              {
                description : '	Is	your	dog	ready	to	try	a	goal?	Set	a	distinct	line	in	front	of	the	net,	such	as the	edge	of	a	concrete	surface	next	to	a	grass	field.	Run	excitedly	with	your dog	and	encourage	him	to	push	the	ball	past	this	line.	When	he	does,	reward him	immediately. ',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                  question : 'MY	DOG	SKINNED	HIS	NOSE! ',
                  response : 'With	a	brand	new	treat	ball,	or	a	very	enthusiastic	roller,	a	dog	can develop	scratches	on	his	nose.	Check	his	nose	often	and	inspect	the	ball for	snags. '
              },
              {
                  question : 'MY	DOG	PAWS	AT	THE	SOCCER	BALL	INSTEAD	OF	ROLLING IT ',
                  response : 'Your	dog	is	frustrated	and	not	understanding	what	you	want.	Go	back	to using	the	treat	ball,	but	put	only	one	kibble	in	it.	Your	dog	will	hear	that there	is	something	in	it,	but	the	kibble	will	take	a	longer	time	to	come out.	Reward	your	dog	for	rolling	it	with	treats	from	your	hand.'
              },
            ]
          }
        },
        // tutorial 2
        {
          title : 'Basketball',
          teachIt : {
            imgSteps : [
              {
                description : 'Toss	a	basketball	for	your	dog	to	fetch.',
              },
              {
                description : '	Coax	him	toward	a	treat	held	against	the	backboard.',
              },
              {
                description : 'As	he	reaches	for	the	treat,	the	ball	should	drop	into	the	net.',
              },
              {
                description : 'Soon	your	dog	will	be	slam	dunking	on	his	own.',
              },
            ]
          }
          ,
          details : {
            description : 'Your	dog	will	slam	dunk	the	competition	when	he	nets	the	basketball.	Add	a second	dog	for	competition,	or	challenge	your	friends! ',
            steps : [
              {
                description : 'Set	the	net	of	a	toy	basketball	stand	low	enough	that	your	dog	can	reach	it while	standing	on	four	paws.	Toss	a	toy	basketball	for	him	to	fetc',
              },
              {
                description : '	Coax	him	toward	a	treat	held	against	the	backboard	while	telling	him	to “dunk.” '
              },
              {
                description : 'As	he	reaches	toward	the	treat,	command	him	to	“drop	it, He should	release	the	ball	into	the	net	as	he	opens	his	mouth	for	the	treat. '
              },
              {
                description : 'At	first,	reward	him	for	dropping	the	ball	anywhere	near	the	net.	As	he improves,	require	a	successful	basket	before	rewarding. ',
              },
              {
                description : 'Challenge	your	dog	further	by	tapping	the	backboard	instead	of	holding	a	treat against	it.	As	your	dog	progresses,	the	verbal	cue	“dunk”	will	come	to	mean the	entire	action	of	fetching	the	ball	and	dropping	it	in	the	net.'
              }
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG’S	BASKETBALL	KEEPS	MISSING	THE	NET ',
                response : 'Your	dog’s	initial	success	in	making	the	basket	is	largely	dependent	on the	timing	and	placement	of	your	reward.	Watch	his	head	and	hold	the treat	in	a	location	that	will	cause	the	ball	to	fall	in	the	net	when	your dog	opens	his	mouth	for	the	treat.'
              },
            ]
          }
        },
        // tutorial 3
        {
          title : 'Hide-and-Seek',
          teachIt : {
            imgSteps : [
              {
                description : ' Position your dog in a sit-stay, and then call him to “come.”',
              },
              {
                description : 'Treat this like a game, and not an obedience drill',
              },
              {
                description : ' Hide just outside the room and call to your dog to “find [your name]!”',
              },
              {
                description : 'Praise your dog for finding you.',
              },
              {
                description : 'Choose more difficult hiding places, such as behind a door.',
              },
            ]
          }
          ,
          details : {
            description : 'Your	dog	holds	a	stay	while	you	find	a	hiding	spot.	Upon	yelling	a	release	word, he	comes	looking	for	you! ',
            steps : [
              {
                description : 'Hide-and-seek	is	a	game,	not	an	obedience	drill.	Make	it	fun	for	your	dog	with high	energy	and	laughter!	Position	your	dog	in	a	sit-stay and walk	to	the	other	side	of	the	room.	Call	your	dog	to	come and reward	him	with	a	treat. ',
              },
              {
                description : 'Again	put	your	dog	in	a	stay,	and	walk	just	outside	the	room.	Call	him enthusiastically	to	“find	[your	name]”	and	praise	and	reward	him	when	he does. '
              },
              {
                description : '	Choose	a	more	difficult	hiding	spot,	such	as	behind	a	door.	Call	to	him	loudly when	you	are	settled	in	your	spot.	Your	dog	will	use	his	keen	canine	nose	to sniff	you	out! '
              },
              {
                description : 'Is	the	game	getting	too	easy	for	your	dog?	He	can	actually	smell	the	path	to your	hiding	spot.	Make	it	more	difficult	by	walking	into	several	rooms	before choosing	your	final	spot. ',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'WHEN	I	LEAVE	THE	ROOM,	MY	DOG	CHEATS	AND	BREAKS	HIS STAY! ',
                response : 'Check	back	on	him	periodically	and	return	him	to	his	original	spot	if	he has	moved.	When	your	housemate	is	cooking	dinner,	set	your	dog	in	the kitchen	so	his	stay	can	be	enforced.'
              },
            ]
          }
        },
        // tutorial 4
        {
          title : 'Go	Hide',
          teachIt : {
            imgSteps : [
              {
                description : 'Toss	a	treat	behind	the	table	and	tell	your	dog	to	“go	hide.”',
              },
              {
                description : 'Wean	off	treats	as	you	point	to	the	table	and	say	“go	hide.”',
              },
              {
                description : 'Reward	your	dog	with	a	toy.',
              },
            ]
          }
          ,
          details : {
            description : 'When	you	tell	your	dog	to	“go	hide,”	he	hides	behind	any	object.	A	big	dog trying	to	hide	behind	a	skinny	pole	is	always	good	for	a	laugh!',
            steps : [
              {
                description : 'This	trick	is	picked	up	easiest	by	toy-motivated	dogs.	Get	your	dog	excited with	a	game	of	fetch.'
              },
              {
                description : 'Set	a	large	object,	such	as	an	upturned	picnic	table,	in	your	play	area.	Show your	dog	a	treat	and	tell	him	to	“go	hide”	as	you	toss	it	behind	the	table.	Praise him	for	going	behind	the	table,	then	immediately	get	his	attention	and	toss	his toy	into	the	yard.	The	toy	serves	as	his	reward,	while	the	treat	is	merely	used to	cause	him	to	go	to	the	correct	place. '
              },
              {
                description : 'Wean	off	the	treats	as	you	just	tell	him	to	“go	hide”	and	point	to	the	table.	Your dog	may	only	go	half	way	to	the	table,	in	which	case	walk	toward	him	as	you keep	pointing	and	cueing.	You	may	even	have	to	walk	all	the	way	to	the	table to	get	him	to	go	behind	it	Don’t	reward	him	with	his	toy	until	he	is	in	the correct	spot.	The	toy	is	his	incentive,	and	the	more	he	wants	it,	the	quicker	he will	learn. '
              },
              {
                description : 'Once	he	is	hiding	behind	the	table,	try	other	objects.	Point	to	a	tree	or	the corner	of	a	building	and	have	him	hide	there. '
              }
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY	DOG	ISN’T	INTERESTED	IN	TOYS ',
                response : 'Is	he	interested	in	treats?	Have	him	hide	and	then	toss	a	treat	to	him.	Be sure	to	toss	the	treat,	rather	than	having	him	come	back	to	you	for	it,	as that	would	encourage	him	to	come	out	of	his	hiding	spot.'
              },
            ]
          }
        },
      ]
    },





    // empty chapter
    {
      title : 'Modern	Conveniences',
      number : 4,
      description :'Contemporary dogs have become full-fledged family members in today’s households; sleeping on beds, wearing clothing, and eating gourmet meals. Skills once required of outdoor dogs have been replaced by a more practicable set of skills geared toward today’s modern living. While a dog’s ability to hunt for your dinner used to be of great importance, it is now more often appreciated when a dog can find the remote control, answer the telephone, and especially bring you a cold one from the fridge! There is something about a dog doing “people things” that we humans find endearing. When we teach a dog to respond to a cue with a natural behavior (such as fetching), we have taught him to associate a word with a particular action. When we teach a dog to execute a “people behavior,” we have taught him not only the word but a complex idea involving logic and noninstinctive physical responses. ',
      tutorials : [
        // tutorial 1
        {
          title : 'Get the Phone When It Rings',
          teachIt : {
            imgSteps : [
               {
                  description : ' Have your dog take the receiver from the floor. ',
              },
              {
                  description : 'Use a second phone to teach your dog to respond to the ring. ',
              },
            ]
          }
          ,
          details : {
            description : 'When the phone rings, your dog will pick it up from its receiver and bring it to you. With a cell phone, your dog will find it and bring it to you. ',
            steps : [
              {
                  description : 'Set your phone on the floor and lift the receiver. Tell your dog to “take it” and reward his effort. ',
              },
              {
                  description : 'Move away from the phone and have him fetch the receiver. Introduce your verbal cue by doing your best imitation of your phone’s ring. Again, reward your dog for a successful retrieve. '
              },
              {
                  description : ' Gradually move the phone back to its original spot—first moving it to a small table, then the counter, then the back of the counter. Your small dog may need a stool to reach the phone. '
              },
              {
                description : 'You’ll now want to associate the actual phone ring with the verbal cue you were using. Use a second phone line to dial your number. When it rings, give your verbal cue and point toward the phone. Your dog may be startled for a second, but cue him each time the phone rings.',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                  question : 'MY DOG DROPS THE PHONE ',
                  response : 'Part of the problem may be the clumsy shape and slippery texture of your phone. Retro phones with a slim handle work well, or you may wish to wrap your phone with tape.'
              }
            ]
          }
        },
        // tutorial 2
        {
          title : 'Turn Off the Light',
          teachIt : {
            imgSteps : [
              {
                description : 'Hold a treat above the light switch and encourage your dog to get it. ',
              },
              {
                description : 'Tap the light switch to cue your dog to paw at it.',
              },
              {
                description : 'Require a successful switch flip before rewarding.',
              },
              {
                description : 'Send your dog to flip the switch on his own! ',
              }
            ]
          }
          ,
          details : {
            description : 'Your dog will learn to paw a light switch on the wall, turning the lights on or off. A flat, rocker light switch is easiest, especially for flipping the switch to the up position. Small dogs may require a stool placed under the switch.',
            steps : [
              {
                description : 'Hold a treat against the wall a little above the light switch and encourage your dog with “lights, get it!” Let him have the treat when he is able to reach the switch.',
              },
              {
                description : 'Hold the treat a little above the switch and away from the wall while tapping the switch with your other hand. Encourage your dog up again, but keep the treat clenched in your fist until he paws once or twice against the wall. Praise him and give him the treat while he is still upright. '
              },
              {
                description : 'Tap the switch plate while cueing your dog, then put your hands down and allow your dog to paw at the wall by himself. As he improves, challenge your ' +
                'dog to make a successful switch flip before he is rewarded. '
              },
              {
                description : 'Finally, stand across the room and send your dog by himself to kill the lights!',
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'HOW DO I TEACH THE DIFFERENCE BETWEEN TURNING THE LIGHT ON AND OFF? ',
                response : 'Your dog will not have the fine motor skills in this position to maneuver the switch one way or the other. He will just paw at the switch until you let him know he was successful. '
              }
            ]
          }
        },
        // tutorial 3
        {
          title : 'Bring Me a Beer from the Fridge ',
          teachIt : {
            imgSteps : [
              {
                description : ' Have your dog pull a dish towel tied to the handle.',
              },
              {
                description : 'His paws should remain on the ground while he pulls. ',
              },
              {
                description : ' Empty a beer can. ',
              },
              {
                description : 'Play fetch with the empty can. '
              },
              {
                description : 'A foam can insulator will make it easier to carry.',
              },
              {
                description : ' Fetch the can from the fridge. ',
              },
              {
                description : 'Reward him for the fetch. '
              },
              {
                description : 'Have him return to close the door.  '
              }
            ]
          }
          ,
          details : {
            description : 'In this useful trick, your dog opens the refrigerator door, fetches a beer, and returns to close the door.',
            steps : [
              {
                description : ' Practice pull on a rope with a dish towel. Tie the dish towel to the refrigerator handle. With the fridge door slightly ajar, instruct your dog to pull the dish towel. All four paws should remain on the floor while your dog pulls —to protect your door as well as to keep him from pushing against himself. Make it more challenging by closing the fridge door completely. '
              },
              {
                description : ' Empty a beer can. '
              },
              {
                description : ' Play fetch with the empty can to get your dog accustomed to carrying it. As many dogs are reluctant to hold metal in their mouths, a foam can insulator may help. '
              },
              {
                description : ' Place the beer can on a low shelf in an open, uncluttered refrigerator and have your dog fetch it. Reward him with a treat tastier than anything he may find in the fridge. '
              },
              {
                description : 'Cue your dog to close the REFRIGERATOR while tapping the front of the open refrigerator door. '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MY FLOOR IS GETTING SCRATCHED UP! ',
                response : 'Lightweight dogs and tile floors are a slippery combination as your dog pulls the dish towel. Improve his traction with a doormat, or use a longer rope on the door handle to increase his angle of leverage.'
              },
              {
                question : 'MY DOG IS BROWSING IN THE FRIDGE WHEN GETTING MY BEER',
                response : 'Nothing is free, and that just might be the price you have to pay for the luxury of beer delivery! '
              },
            ]
          }
        },
        // tutorial 4
        {
          title : 'Find the Car Keys',
          teachIt : {
            imgSteps : [
              {
                description : ' Fill a key chain pouch with treats. Reward your dog for fetching it.',
              },
              {
                description : ' Hide the keys and help your dog search for them. ',
              },
            ]
          }
          ,
          details : {
            description : 'Your dog will locate and retrieve your missing items. What a useful trick! ',
            steps : [
              {
                description : 'Attach a small change purse filled with treats to your key chain. Toss the keys playfully and tell your dog “keys, fetch”. When he returns with the keys, open the pouch and reward him with a treat from inside. As he cannot open the pouch on his own, he will learn to bring it hurriedly to you. The scent of the treats in the change purse will help your dog find your keys. '
              },
              {
                description : 'Next, hide the keys farther away, or in the next room. Make a game of it and help your dog search room to room. The next time you lose your keys, you’ll be glad you put in the effort of teaching this trick! '
              },
            ]
          }
          ,
          troublshooting : {
            questionsResponses : [
              {
                question : 'MUST I KEEP THE TREAT POUCH PERMANENTLY ON MY KEY CHAIN',
                response : 'You can phase it out over time, but an object with a distinct scent will be easier for your dog to find. Rubber or leather key chains can do the trick. '
              },
            ]
          }
        },
      ]
    }
]
};
