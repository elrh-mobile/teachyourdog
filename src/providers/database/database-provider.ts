import {SQLiteObject} from "@ionic-native/sqlite";

export abstract class DatabaseProvider {
  dbFile : string ='teach-your-dog-db.db';

  /**
   * Ouvrir la base de données et retourne l'objet de la bd
   * @return {Promise<SQLiteObject>}
   */
  abstract openDataBase() : Promise<SQLiteObject>;

  /**
   * retourne la liste des requettes permettant d'initialiser la base de données
   */
  abstract getInsertDataAsArray();
  /**
   * Initialise la base de données de l'application en cas de premiere utilisation de l'app
   * et retourne une promesse de type boolean
   * @return {Promise<boolean>}
   */
  abstract initDatabase() : Promise<boolean>;

  /**
   * return true if tables are created
   * @return {Promise<boolean>}
   */
  abstract isReadyTables () : Promise<boolean>;

  /**
   * check if the db is set (using version in implementation)
   * @return {Promise<boolean>}
   */
  abstract isReadyDatabase () : Promise<boolean>
  //
}
