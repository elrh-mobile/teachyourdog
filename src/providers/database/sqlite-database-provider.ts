import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {DatabaseProvider} from "./database-provider";
import {TeachYourDogData} from "./data";
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import { Storage } from '@ionic/storage';
import {CREATE_TABLES_ARRAY} from "./script-create-tables";

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SqliteDatabaseProvider extends DatabaseProvider{
  private appVersion : any = 1.0;
  private database: SQLiteObject;
//**********************************************************************************************************************
  constructor(private sqlite: SQLite,public sqlitePorter: SQLitePorter, private storage: Storage) {
    super();
  }
//**********************************************************************************************************************
  async openDataBase(): Promise<SQLiteObject> {
    if(this.database != null)
      return Promise.resolve(this.database);
    this.database = await this.sqlite.create({
      name: this.dbFile,
      location: 'default'
    });
    return this.database;
  }
//**********************************************************************************************************************
  private getCreateTablesQuerys () : string []{
    return CREATE_TABLES_ARRAY;
  }
//**********************************************************************************************************************
  private getChapterQuery(id : number,number : number,title : string,description : string) : string {
    return 'INSERT INTO `chapter` (id,number,title,description) VALUES ' +
      '(' + id + ',' +
         number + ',' +
      '\''+title+'\',' +
      '\''+description+'\'' +
      ');';
  }
//**********************************************************************************************************************
  private getTutorialQuery(id : number,number : number,title : string,id_chapter : number) {
    return 'INSERT INTO `tutorial` (id,number,title,id_chapter) VALUES ' +
      '(' + id + ',' +
        number + ','+
      '\''+title+'\',' +
       +id_chapter+
      ');';
  }
//**********************************************************************************************************************
  private getTeachItQuery(id : number,id_tutorial : number){
    return 'INSERT INTO `teach_it` (id,id_tutorial) VALUES' +
      '(' + id + ',' +
          + id_tutorial+
      ');';
  }
//**********************************************************************************************************************
  private getImageStepQuery(number: number,description:string, id_teach_it) {
    return 'INSERT INTO `IMAGE_STEP` (number,description,id_teach_it) VALUES ' +
      '(' + number + ',' +
      '\''+description+'\',' +
          +id_teach_it+
      ');';
  }
//**********************************************************************************************************************
  private getStepQuery(number: number,description:string, id_details) {
    return 'INSERT INTO `STEP` (number,description,id_details) VALUES ' +
      '(' + number + ',' +
      '\''+description+'\',' +
      +id_details+
      ');';
  }
//**********************************************************************************************************************
  private getDetailsQuery(id : number,id_tutorial : number,description:string){
    return 'INSERT INTO `details` (id,id_tutorial,description) VALUES' +
      '(' + id + ',' +
      + id_tutorial+
      ',\''+description+'\'' +
      ');';
  }
//**********************************************************************************************************************
  private getTroubelshootingQuery(id : number,id_tutorial : number,description:string){
    return 'INSERT INTO `Troubleshooting` (id,id_tutorial,description) VALUES' +
      '(' + id + ',' +
      + id_tutorial+
      ',\''+description+'\'' +
      ');';
  }
//**********************************************************************************************************************
  private getQRQuery(id: number,question : string,response : string, id_troubleshooting){
    return 'INSERT INTO `question_response` (id,question,response,troubleshooting_id) VALUES ' +
      '(' + id + ',' +
      '\''+ question +'\',' +
      '\''+ response +'\','
          + id_troubleshooting +
      ');';
  }
//**********************************************************************************************************************
  getInsertDataAsArray () : string [ ]{
    let querys = [];
    let chapters = TeachYourDogData.chapters;
    let cCompteur = 0;
    let tCompteur = 0;
    let tutoNumberCompteur = 0;
    let teachItCompteur = 0;
    let imgStepsCompteur = 0;
    let stepsCompteur = 0;
    let detailsCompteur = 0;
    let troublshotingCompteur = 0;
    let questionResponseCompteur = 0;
    // each chapters
    chapters.forEach((chapter) => {
      cCompteur++;
      querys.push(this.getChapterQuery(cCompteur,chapter.number,chapter.title,chapter.description));
      //querys.push('DELETE FROM tutorial where id_chapter='+cCompteur);
      // each tutorial
        tutoNumberCompteur = 0;
        chapter.tutorials.forEach((tutorial) => {
          tCompteur++;
          tutoNumberCompteur++;
          querys.push(this.getTutorialQuery(tCompteur,tutoNumberCompteur,tutorial.title,cCompteur));
          // teachIt
          //querys.push('DELETE FROM teach_it where id_tutorial='+tCompteur);
          teachItCompteur++;
          querys.push(this.getTeachItQuery(teachItCompteur,tCompteur));
            // foreach imgSteps
          //querys.push('DELETE FROM IMAGE_STEP where id_teach_it='+teachItCompteur);
          imgStepsCompteur = 0;
            tutorial.teachIt.imgSteps.forEach((imgStep) => {
              imgStepsCompteur++;
              querys.push(this.getImageStepQuery(imgStepsCompteur,imgStep.description,teachItCompteur));
            });
          // details
          //querys.push('DELETE FROM details where id_tutorial='+tCompteur);
          detailsCompteur++;
          querys.push(this.getDetailsQuery(detailsCompteur,tCompteur,tutorial.details.description));
          //querys.push('DELETE FROM STEP where id_details='+detailsCompteur);
            // forach steps
            stepsCompteur = 0;
            tutorial.details.steps.forEach((step) => {
                stepsCompteur++;
                querys.push(this.getStepQuery(stepsCompteur,step.description,detailsCompteur));
            });
         //querys.push('DELETE FROM Troubleshooting where id_tutorial = '+tCompteur);
         troublshotingCompteur++;
         querys.push(this.getTroubelshootingQuery(troublshotingCompteur,tCompteur,null));
         tutorial.troublshooting.questionsResponses.forEach((qr) => {
            questionResponseCompteur++;
            querys.push(this.getQRQuery(questionResponseCompteur,qr.question,qr.response,troublshotingCompteur));
         })
        })
    });
    console.log(querys);
    return querys;
  }
//**********************************************************************************************************************
  public async initDatabase () : Promise<boolean>{
    try {
      // open db
      await this.openDataBase();
      // create tables
      if( !(await this.isReadyTables())){
        // creation des tables
        for(let requete of this.getCreateTablesQuerys()){
          try {
            await  this.sqlitePorter.importSqlToDb(this.database, requete);
            await this.storage.set('readyTables',true);
          }
          catch (e){
            await this.storage.set('readyTables',false);
          }
        }
      }
      // insert db data
      if ((await this.isReadyTables()) &&  !(await this.isReadyDatabase()) ) {
        for(let requete of this.getInsertDataAsArray()) {
          // insertion des requetes
          await  this.sqlitePorter.importSqlToDb(this.database, requete);
        };
        // database state is OK
        await this.setDatabaseState(true);
      }
      return Promise.resolve(true);
    } catch (e) {
      console.log(e);
      await this.setDatabaseState(false);
      return Promise.resolve(false);
    }
  }
//**********************************************************************************************************************
  public async isReadyDatabase () : Promise<boolean> {
    try {
      // check if db is set
      let readyDatabase = await this.storage.get('readyDatabase');
      if(readyDatabase == null || readyDatabase == false)
         return Promise.resolve(false);
      // check the version
      let appV =  await this.storage.get('appVersion');
      if(appV == null || appV != this.appVersion)
         return Promise.resolve(false);
      // db is ready
      return Promise.resolve(true);
    } catch (e) {
      return Promise.resolve(false);
    }
  }
//**********************************************************************************************************************
  public async isReadyTables () : Promise<boolean> {
    try {
      // check if tables are created
      let readyTables = await this.storage.get('readyTables');
      if(readyTables == null || readyTables == false)
        return Promise.resolve(false);
      // db is ready
      return Promise.resolve(true);
    } catch (e) {
      return Promise.resolve(false);
    }
  }
//**********************************************************************************************************************
  private async setDatabaseState ( stat : boolean){
    try {
      await this.storage.set('readyDatabase',stat);
      await this.storage.set('appVersion',this.appVersion);
    } catch (e) {
      console.log(e);
    }
  }
}
