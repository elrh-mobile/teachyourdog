import { Injectable } from '@angular/core';
import {ITutorialService} from "./tutorial-interface";
import {Tutorial} from "../../classes/tutorial";
import {TeachIt} from "../../classes/teach-it";
import {ImageStep} from "../../classes/image-step";
import {TutorialDetails} from "../../classes/tutorial-details";
import {Step} from "../../classes/step";
import {Troubleshooting} from "../../classes/troubleshooting";
import {QuestionResponse} from "../../interfaces/question-response";
import {DatabaseProvider} from "../database/database-provider";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Chapter} from "../../classes/chapter";

/*
  Generated class for the TutorialProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StaticTutorialProvider extends ITutorialService{
//**********************************************************************************************************************
  private tuto : Tutorial;
  private tutoNavParam = null;
//**********************************************************************************************************************
  constructor(public databaseProvider :  DatabaseProvider) {
    super();
    this.tuto = new Tutorial();
    //
    this.tuto.title = 'Sit';
    this.tuto.id = 1;
    this.tuto.img = 'https://image.ibb.co/m8uWZb/output_Image_021.jpg';
    this.tuto.teachIt = new TeachIt();
    // steps Teach It
    let s1 : ImageStep = new ImageStep();
    s1.number = 1;
    s1.description = 'Hold a treat over your dog’s head.';
    s1.img = 'https://image.ibb.co/nLXVeb/output_Image_024.jpg';

    let s2 : ImageStep = new ImageStep();
    s2.number = 2;
    s2.description = 'Move it straight back.';
    s2.img = 'https://image.ibb.co/f0xJXw/output_Image_025.jpg';

    let s3 : ImageStep = new ImageStep();
    s3.number = 3;
    s3.description = 'Press his haunches while pulling up on the leash.';
    s3.img = 'https://image.ibb.co/bva5Cw/output_Image_026.jpg';

    this.tuto.teachIt.steps = [];
    this.tuto.teachIt.steps.push(s1,s2,s3);
    // steps for details
    this.tuto.tutorialDetails = new TutorialDetails();
    this.tuto.tutorialDetails.description = 'Your dog sits squarely on his hindquarters and remains there until released.';
    this.tuto.tutorialDetails.steps = [];

    let s11 : Step = new Step();
    s11.number = 1;
    s11.description = 'Stand or kneel in front of your dog, holding a treat in your hand a little higher ' +
                      'than your dog’s head.';

    let s22 : Step = new Step();
    s22.number = 2;
    s22.description = 'Slowly move the treat straight back over your dog’s head. This should cause ' +
                      'his nose to point up and his rear to drop. If his rear does not drop, keep ' +
                      'moving the treat straight backward toward his tail. The instant his rear touches ' +
                      'the floor, release the treat and mark the behavior by saying “good sit! ';


    let s33 : Step = new Step();
    s33.number = 3;
    s33.description = 'If your dog is not responding to the food lure, use your index finger and thumb ' +
                      'to put pressure on either side of his haunches, just forward of his hip bones. ' +
                      'Pull up on his leash at the same time to rock him back into a sit. Praise and ' +
                      'reward him while he is sitting.';

    let s44 : Step = new Step();
    s44.number = 4;
    s44.description = 'Once your dog is consistently sitting, wait a few seconds before rewarding.' +
                      'Remember to only reward while your dog is in the correct position—sitting.';

    this.tuto.tutorialDetails.steps.push(s11,s22,s33,s44);

    //
    this.tuto.troubleshooting = new Troubleshooting();
    let q1 = new QuestionResponse();
    q1.question = 'My Dog Jumps At My Hand With The Treat';
    q1.response = 'Hold the treat lower, so that he can reach it while standing.';
    let q2 = new QuestionResponse();
    q2.question = 'MY DOG SITS, BUT KEEPS GETTING UP';
    q2.response = 'In a gentle but firm manner, keep placing your dog back in a sit. Once ' +
                   'he has learned the behavior, he should not break his sit until released.';
    this.tuto.troubleshooting.qrs.push(q1,q2);
  }
//**********************************************************************************************************************
  async setTeachItOfTutorial(tutorial: Tutorial){
    let db : SQLiteObject = await this.databaseProvider.openDataBase();
    let resultSet  = await
                    db.executeSql('SELECT * FROM  TEACH_IT WHERE id_tutorial =' + tutorial.id, []);
        if(resultSet.rows != null && resultSet.rows.length > 0){
          console.log('we get teach it of tuto');
          tutorial.teachIt = this.getMinimalTeachItFromResultSetRow(resultSet.rows.item(0));
          console.log(tutorial.teachIt);
          tutorial.teachIt.tutorial = tutorial;
          await this.setImsgsStepsOfTeachIt(tutorial.teachIt);
        }
  }
//**********************************************************************************************************************
  async setDetailsOfTutorial(tutorial: Tutorial){
    let db : SQLiteObject = await this.databaseProvider.openDataBase();
    let resultSet  = await
    db.executeSql('SELECT * FROM  details WHERE id_tutorial =' + tutorial.id, []);
    if(resultSet.rows != null && resultSet.rows.length > 0){
      console.log('we get details  of tuto');
      tutorial.tutorialDetails = new TutorialDetails();
      tutorial.tutorialDetails.id = resultSet.rows.item(0).id;
      tutorial.tutorialDetails.description = resultSet.rows.item(0).description;
      tutorial.tutorialDetails.tutorial = tutorial;
      console.log(tutorial.tutorialDetails);
      await this.setStepsOfDetails(tutorial.tutorialDetails);
    }
  }
//**********************************************************************************************************************
  async setTroublshootingOfTutorial(tutorial: Tutorial){
    try {
      let db: SQLiteObject = await this.databaseProvider.openDataBase();
      let resultSet = await
        db.executeSql('SELECT * FROM  Troubleshooting WHERE id_tutorial =' + tutorial.id, []);
      if (resultSet.rows != null && resultSet.rows.length > 0) {
        tutorial.troubleshooting = new Troubleshooting();
        tutorial.troubleshooting.id = resultSet.rows.item(0).id;
        tutorial.troubleshooting.tutorial = tutorial;
        await this.setQuestionsResponseOfTroublshooting(tutorial.troubleshooting);
      }
    } catch (e) {
      console.log(e);
    }
  }
//**********************************************************************************************************************
  getTutorial(id: number) {
    return this.tuto;
  }
//**********************************************************************************************************************
  getMinimalTutorialsFromResultSet(resultSet: any,chapter : Chapter = null) {
    if (resultSet != null && resultSet.rows.length > 0) {
      let tutorials : Tutorial [] = [];
      for (let i = 0; i < resultSet.rows.length; i++) {
        tutorials.push(this.getMinimalTutorialFromResultSetRow(resultSet.rows.item(i),chapter));
      }
      return tutorials;
    }
  }
//**********************************************************************************************************************
  private getMinimalTutorialFromResultSetRow(row: any,chapter : Chapter = null) : Tutorial {
    try {
      let t = new Tutorial();
      t.id = row.id;
      t.title = row.title;
      t.number = row.number;
      t.chapter = chapter;
      return t;
    } catch (e) {
      return null;
    }
  }
//*********************************************************************************************************************
  private getMinimalTeachItFromResultSetRow(row: any) : TeachIt {
    try {
      let t = new TeachIt();
      t.id = row.id;
      return t;
    } catch (e) {
      return null;
    }
  }
//**********************************************************************************************************************
  private async setImsgsStepsOfTeachIt(teachIt : TeachIt){
        let db : SQLiteObject  = await this.databaseProvider.openDataBase();
        let resultSet : any =
                await db.executeSql('SELECT * FROM  IMAGE_STEP WHERE id_teach_it =' + teachIt.id, []);
          if(resultSet.rows != null && resultSet.rows.length > 0){
            teachIt.steps = [];
            for (let  i = 0 ; i< resultSet.rows.length ; i++){
              let imgStep = new ImageStep();
              imgStep.number = resultSet.rows.item(i).number;
              imgStep.description = resultSet.rows.item(i).description;
              imgStep.teachIt = teachIt;
              teachIt.steps.push(imgStep);
            }
          }
  }
//**********************************************************************************************************************
  private async setStepsOfDetails(details : TutorialDetails) {
    let db: SQLiteObject = await this.databaseProvider.openDataBase();
    let resultSet: any =
      await db.executeSql('SELECT * FROM  STEP WHERE id_details =' + details.id, []);
    if (resultSet.rows != null && resultSet.rows.length > 0) {
      details.steps = [];
      for (let i = 0; i < resultSet.rows.length; i++) {
        let step = new Step();
        step.number = resultSet.rows.item(i).number;
        step.description = resultSet.rows.item(i).description;
        step.tutoDetails = details;
        details.steps.push(step);
      }
    }
  }
//**********************************************************************************************************************
  private async setQuestionsResponseOfTroublshooting(troubl : Troubleshooting) {
    let db: SQLiteObject = await this.databaseProvider.openDataBase();
    let resultSet: any =
      await db.executeSql('SELECT * FROM  question_response WHERE troubleshooting_id =' + troubl.id, []);
    if (resultSet.rows != null && resultSet.rows.length > 0) {
      troubl.qrs = [];
      for (let i = 0; i < resultSet.rows.length; i++) {
        let qr = new QuestionResponse();
        qr.id = resultSet.rows.item(i).id;
        qr.question = resultSet.rows.item(i).question;
        qr.response = resultSet.rows.item(i).response;
        qr.troublshooting = troubl;
        troubl.qrs.push(qr);
      }
    }
  }
//**********************************************************************************************************************
public setTutoNavParam(tutorial : Tutorial){
  this.tutoNavParam = tutorial;
}
//**********************************************************************************************************************
public  getTutoNavParam() : Tutorial{
 return this.tutoNavParam;
}
//**********************************************************************************************************************
  async setTutotialsOfChapter(chapter: Chapter) {
    if(chapter == null)
      return false;
    let db = await this.databaseProvider.openDataBase();
    let resultSet =
      await db.executeSql('SELECT * FROM TUTORIAL where id_chapter = '+chapter.id,[]);
    chapter.tutorials = this.getMinimalTutorialsFromResultSet(resultSet,chapter);
  }
//**********************************************************************************************************************
}
