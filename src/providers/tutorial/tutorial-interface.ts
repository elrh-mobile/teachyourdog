import { Tutorial } from './../../classes/tutorial';
import {Chapter} from "../../classes/chapter";

export abstract class ITutorialService {
  abstract getTutorial(id :number);
  abstract setTeachItOfTutorial(tutorial : Tutorial);
  abstract setDetailsOfTutorial(tutorial : Tutorial);
  abstract setTroublshootingOfTutorial(tutorial : Tutorial);
  abstract setTutoNavParam(tutorial : Tutorial);
  abstract getTutoNavParam() : Tutorial;
  abstract setTutotialsOfChapter(chapter : Chapter);
}
